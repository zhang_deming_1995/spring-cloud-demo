package com.dm.cloud.excelpojo;

import com.dm.cloud.utils.mingexcel.entity.TransferItem;
import com.dm.cloud.utils.mingexcel.medium.TransferItemArrayInitializer;

public class TestExcelDropDownItems implements TransferItemArrayInitializer {
    @Override
    public TransferItem[] initItems() {

        TransferItem[] result = new TransferItem[3];
        TransferItem item1 = new TransferItem();
        item1.setKey("1");
        item1.setValue("一");
        result[0] = item1;

        TransferItem item2 = new TransferItem();
        item2.setKey("2");
        item2.setValue("er");
        result[1] = item2;

        TransferItem item3 = new TransferItem();
        item3.setKey("3");
        item3.setValue("叁");
        result[2] = item3;

        return result;
    }
}
