package com.dm.cloud.excelpojo;

import com.dm.cloud.utils.mingexcel.annotions.*;
import com.dm.cloud.utils.mingexcel.annotions.innerannotation.Font;
import com.dm.cloud.utils.mingexcel.annotions.innerannotation.*;
import lombok.Data;
import org.apache.poi.ss.usermodel.*;

import java.io.Serializable;

@Data
@ExcelProperties(
        row = @RowProperties(height = 300)
        ,sheetName = "测试表格"
        ,title = @Title(row = @RowProperties(height = 1000), title = "别的就啥都巴萨恨不得撒谎基本大数据",properties = @CellProperties(border = @Border(showBorder = true,left = BorderStyle.DASH_DOT,top = BorderStyle.DOUBLE)
            , horizontalAlignment = HorizontalAlignment.CENTER, verticalAlignment = VerticalAlignment.CENTER, font = @Font(fontName = "微软雅黑",bold = true,height = 600)))
        ,mergedHeaders = {
                @MergedHeaders(row = @RowProperties(height = 600), merge = {
                        @MergedColumnHeader(headerName = "合并列一",from = 0 ,to = 1,properties =@CellProperties(border = @Border(showBorder = true))) ,
                        @MergedColumnHeader(headerName = "合并列二",from = 2 ,to = 2,properties =@CellProperties(border = @Border(showBorder = true)))}
                        ),
        }

)
public class TestExcelPojo2 implements Serializable {
    @ColumnProperties(headerName = "测试字段",rowsSameMerge = true, autoWidth = true)
    @CellProperties(
            border = @Border(showBorder = true,left = BorderStyle.DASH_DOT,top = BorderStyle.DOUBLE),
            fillForegroundColor = IndexedColors.AQUA,
            fillPattern = FillPatternType.SOLID_FOREGROUND,
            horizontalAlignment = HorizontalAlignment.CENTER,
            verticalAlignment = VerticalAlignment.CENTER,
            font = @Font(fontName = "微软雅黑")
    )
    private String testData;

    @DropdownProperties(clazz = TestExcelDropDownItems.class)
    @ColumnProperties(index = 1, headerName = "测试字段1巴哈是不会加大说不定就阿塞拜疆卡萨丁",rowsSameMerge = true, autoWidth = true)
    @CellProperties(
            border = @Border(showBorder = true,left = BorderStyle.DASH_DOT,top = BorderStyle.DOUBLE)
    )
    private String testData1;

    @ColumnProperties(index = 2, headerName = "测试字段2jskabdadasnajkmsbdjkas",rowsSameMerge = true, autoWidth = true)
    @CellProperties(
            border = @Border(showBorder = true,left = BorderStyle.DASH_DOT,top = BorderStyle.DOUBLE),
            wrapText = true
    )
    private String testData2;
}
