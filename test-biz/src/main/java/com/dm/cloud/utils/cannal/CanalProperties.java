package com.dm.cloud.utils.cannal;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 应用启动之后开始干活
 * 官方例子
 */
@Data
@Component
@ConfigurationProperties(prefix = "custom.canal")
public class CanalProperties {

    private String  ip;
    private Integer port;
    private String  destination;
    private String  username;
    private String  password;
    private int     waitTime ;
    private String  subscribe;

}  