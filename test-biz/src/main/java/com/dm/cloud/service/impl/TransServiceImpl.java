package com.dm.cloud.service.impl;

import com.dm.cloud.dao.TestDao;
import com.dm.cloud.service.ITransService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransServiceImpl implements ITransService {

    @Autowired
    TestDao testDao;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void insertB() {
        testDao.insertB("b");
        System.out.println(10/0);
    }

    @Override
    @Transactional
    public void transTerst() {
        try {
            testDao.insertA("a");
        }catch (Exception ex){
//            DO NOTHING
//            throw new RuntimeException(ex.getMessage());
        }
    }
}
