package com.dm.cloud.service;

import com.dm.cloud.common.R;
import org.springframework.web.multipart.MultipartFile;

public interface ITestService {

    boolean redisLock();

    boolean zkLock(Integer e);

    /* 多数据源测试 */
    R<Object> multiDbTest(String dbmss, String sd, String db);

    /* 文件上传测试 */
    R<Object> minioUploud(MultipartFile file);

    void transTerst2();

}
