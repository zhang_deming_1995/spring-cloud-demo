package com.dm.cloud.controller;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

@ControllerAdvice
public class MyInitBinderConfig {

    @InitBinder("car")
    public void initCar(WebDataBinder webDataBinder) {
        webDataBinder.setFieldDefaultPrefix("car.");
    }

    @InitBinder("user")
    public void initUser(WebDataBinder webDataBinder) {
        webDataBinder.setFieldDefaultPrefix("user.");
    }

}