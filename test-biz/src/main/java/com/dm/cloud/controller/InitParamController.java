package com.dm.cloud.controller;

import com.dm.cloud.common.R;
import com.dm.cloud.pojo.Car;
import com.dm.cloud.pojo.User;
import com.dm.cloud.service.ITestService;
import com.dm.cloud.service.ITransService;
import com.dm.cloud.utils.CodeFormater;
import com.dm.cloud.utils.minio.MinioUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Log4j2
@RestController
@RequestMapping("/init_param")
public class InitParamController {

    @GetMapping("/test1")
    public R test1(@ModelAttribute("car") Car car){
        return R.success(car.getModel()+":"+car.getNum());
    }

    @GetMapping("/test2")
    public R test2(User user){
        return R.success(user.getName()+":"+user.getAge());
    }
}
