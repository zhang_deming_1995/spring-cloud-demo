package com.dm.cloud.controller;

import com.dm.cloud.common.R;
import com.dm.cloud.databases.redis.RedissionUtils;
import com.dm.cloud.excelpojo.TestExcelPojo;
import com.dm.cloud.excelpojo.TestExcelPojo2;
import com.dm.cloud.service.ITestService;
import com.dm.cloud.service.ITransService;
import com.dm.cloud.utils.CodeFormater;
import com.dm.cloud.utils.mingexcel.MingExcelUtil;
import com.dm.cloud.utils.minio.MinioUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.util.Strings;
import org.checkerframework.checker.units.qual.A;
import org.redisson.RedissonMultiLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Log4j2
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    ITestService testService;

    @Autowired
    ITransService transService;

    @GetMapping("/getTest")
    public void getTest(HttpServletResponse response) throws IllegalAccessException, InstantiationException, InvocationTargetException {
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//        String pwd = encoder.encode("nacos");
//        System.out.println(pwd);
        List<TestExcelPojo> list =new ArrayList<>();
        TestExcelPojo pojo1 = new TestExcelPojo();
        pojo1.setTestData("1");
        pojo1.setTestData1("1");
        pojo1.setTestData2("111");
        list.add(pojo1);
        TestExcelPojo pojo2 = new TestExcelPojo();
        pojo2.setTestData("2");
        pojo2.setTestData1("2");
        pojo2.setTestData2("222");
        list.add(pojo2);
        TestExcelPojo pojo3 = new TestExcelPojo();
        pojo3.setTestData("3");
        pojo3.setTestData1("3");
        pojo3.setTestData2("333");
        list.add(pojo3);

        List<TestExcelPojo2> list2 =new ArrayList<>();
        TestExcelPojo2 pojo12 = new TestExcelPojo2();
        pojo12.setTestData("1");
        pojo12.setTestData1("1");
        pojo12.setTestData2("111");
        list2.add(pojo12);
        TestExcelPojo2 pojo22 = new TestExcelPojo2();
        pojo22.setTestData("2");
        pojo22.setTestData1("2");
        pojo22.setTestData2("222");
        list2.add(pojo22);
        TestExcelPojo2 pojo32 = new TestExcelPojo2();
        pojo32.setTestData("3");
        pojo32.setTestData1("3");
        pojo32.setTestData2("333");
        list2.add(pojo32);

        new MingExcelUtil.Builder().addSheetWriter(TestExcelPojo.class,list).addSheetWriter(TestExcelPojo2.class,list2).build().exportExcel(response);
    }

    @PostMapping("/postText")
    public void getTest(@RequestParam("file") MultipartFile multipartFile,HttpServletResponse response) throws Exception {
        List<TestExcelPojo> result = new ArrayList<>();
        StopWatch stopWatch = new StopWatch();
        MingExcelUtil util = new MingExcelUtil.Builder()
                .setSheetReaderClass(TestExcelPojo.class)
                .addSheetWriter(TestExcelPojo.class,result)
                .build();
        stopWatch.start("read");
        result.addAll(util.importExcel(multipartFile,0));
        stopWatch.stop();
        stopWatch.start("write");
        util.exportExcel(response);
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
    }

    /**
     * 异步
     * @return
     */
    @GetMapping("async")
    public void async(HttpServletRequest request, HttpServletResponse response){

        //开启异步
        AsyncContext asyncContext =request.startAsync();
        CompletableFuture.runAsync(()-> {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                response.getWriter().append("nihao");
            } catch (IOException e) {
                e.printStackTrace();
            }
            asyncContext.complete();
        });
    }

    @GetMapping("/redistrylock")
    public int search(){

        IntStream.rangeClosed(1,6).parallel().boxed().forEach(
                e->{
                    try {
                        log.info(String.format(" 线程 %s，加锁结果 %s", Thread.currentThread().getName(), testService.redisLock()));
                    }catch (Exception ex){
                        log.error(ex.getMessage());
                    }
                });
        return 0;
    }


    @GetMapping("/redissionlock")
    public int redissionlock(){

        return 0;
    }

    @GetMapping("/zklock")
    public int zklock(){

//        testService.zkLock();

        IntStream.rangeClosed(1,6).parallel().boxed().forEach(
                e->{
                    try {
                        testService.zkLock(e);
                    }catch (Exception ex){
                        log.error(ex.getMessage());
                    }
                });
        return 0;
    }

    @GetMapping("/delzknode")
    public int delzknode(){

        return 0;
    }

    @GetMapping("/Db/{db}")
    public R<Object> search(@PathVariable("db") String db){
        R<Object> o= testService.multiDbTest("","",db);
        log.info("database [{}] tables {}",db,o.getResult());
        return o;
    }

    @PostMapping("/minio/uploud")
    public R<Object> uploud(@RequestParam("file") MultipartFile file) {

        try {
            return R.success(MinioUtils.upload(file.getInputStream(),file.getContentType()));
        }catch (Exception ex){
            log.error("解析失败");
            return R.failure("",ex.getMessage());
        }
    }

    @GetMapping("/minio/download")
    public R<Object> uploud(@RequestParam(value = "minioLoc",required = false) String minioLoc, @RequestParam("bucket") String bucket, @RequestParam("fileName") String fileName) {
        try {

            Map<String,String> sd =new HashMap<>();
            if(Strings.isNotBlank(minioLoc))
                sd.put("minioLoc",minioLoc);
            sd.put("bucket",bucket);
            sd.put("fileName",fileName);

            return R.success(CodeFormater.getBase64FromInputStream(MinioUtils.getMinio(sd).body().byteStream()));
        }catch (Exception ex){
            log.error("解析失败");
            return R.failure("",ex.getMessage());
        }
    }

    @GetMapping("/transaction/test")
    public void transactionTest() {
        transService.transTerst();
    }
}
