package com.dm.cloud;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.util.Matrix;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.web.bind.annotation.RestController;

import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@RestController
public class TestServiceApplication {
    public static void main(String[] args) throws IOException {
        SpringApplication.run(TestServiceApplication.class, args);
    }
}