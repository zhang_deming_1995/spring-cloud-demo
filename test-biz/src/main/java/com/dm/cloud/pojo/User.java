package com.dm.cloud.pojo;

import lombok.Data;

@Data
public class User {
    String name;
    Integer age;
}
