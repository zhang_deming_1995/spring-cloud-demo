package com.dm.cloud.pojo;

import lombok.Data;

@Data
public class Car {
    String model;
    Integer num;
}
