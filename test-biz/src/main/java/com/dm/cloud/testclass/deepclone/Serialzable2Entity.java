package com.dm.cloud.testclass.deepclone;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class Serialzable2Entity implements Serializable {
    private String stringValue;
    private SerialzablePointerEntity pointerEntity;
    private List<String> listValue=  new ArrayList<>();

    /**
     * 初始化列表
     */
    public void initListValue() {
        for (int i = 0; i < 10000; i++) {
            this.listValue.add(UUID.randomUUID().toString());
        }
    }
}