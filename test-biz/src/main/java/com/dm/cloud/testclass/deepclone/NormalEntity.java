package com.dm.cloud.testclass.deepclone;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class NormalEntity{
    private String stringValue;
    private NormalPointerEntity pointerEntity;
    private List<String> listValue=  new ArrayList<>();

    /**
     * 初始化列表
     */
    public void initListValue() {
        for (int i = 0; i < 10000; i++) {
            this.listValue.add(UUID.randomUUID().toString());
        }
    }
}