package com.dm.cloud.testclass.deepclone;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PointerEntity implements Cloneable{
    private String uuid;

    public PointerEntity(){
    }

    public PointerEntity(String uuid){
        this.uuid = uuid;
    }

    @Override
    protected PointerEntity clone() throws CloneNotSupportedException {
        return (PointerEntity)super.clone();
    }
}