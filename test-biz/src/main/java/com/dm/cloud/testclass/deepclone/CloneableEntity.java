package com.dm.cloud.testclass.deepclone;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class CloneableEntity implements Cloneable{
    private String stringValue;
    private PointerEntity pointerEntity;
    private List<String> listValue=  new ArrayList<>();

    /**
     * 初始化列表
     */
    public void initListValue() {
        for (int i = 0; i < 10000; i++) {
            this.listValue.add(UUID.randomUUID().toString());
        }
    }

    @Override
    public CloneableEntity clone() throws CloneNotSupportedException {
        CloneableEntity clone = (CloneableEntity)super.clone();
//        指针类型的对象也要做一份拷贝 获取新创建一个对象
        if(null!=this.pointerEntity) {
            clone.setPointerEntity(this.pointerEntity.clone());
        }
        clone.setListValue(new ArrayList<>(listValue));
        return clone;
    }
}