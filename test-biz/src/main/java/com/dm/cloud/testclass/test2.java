package com.dm.cloud.testclass;

import com.dm.cloud.testclass.deepclone.*;
import com.dm.cloud.utils.entity.DeepCloneUtils;
import com.google.gson.Gson;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.util.StopWatch;

import java.io.*;
import java.util.ArrayList;
import java.util.UUID;

public class test2 {
    public static void main(String[] args) throws CloneNotSupportedException, IOException, ClassNotFoundException {

        StopWatch stopWatch = new StopWatch();
//        Cloneable 深拷贝
        CloneableEntity entity1 = new CloneableEntity();
        entity1.initListValue();
        stopWatch.start("Cloneable 深拷贝");
        CloneableEntity clone1 = entity1.clone();
        stopWatch.stop();

//        Apache Commons Lang序列化深拷贝 拷贝对象和对象中所有涉及到的指针类型的对象都要实现序列化
        Serialzable2Entity entity2 = new Serialzable2Entity();
        entity2.initListValue();
        stopWatch.start("Apache Commons Lang序列化深拷贝");
        Serialzable2Entity clone2 = SerializationUtils.clone(entity2);
        stopWatch.stop();

//        Gson 序列化反序列化深拷贝 对对象没有要求
        NormalEntity entity3 = new NormalEntity();
        entity3.initListValue();
        stopWatch.start(" Gson 序列化反序列化深拷贝");
        Gson gson = new Gson();//用来打印对象
        NormalEntity clone3 = gson.fromJson(gson.toJson(entity3 ), NormalEntity.class);
        stopWatch.stop();

        //        流深拷贝
        Serialzable2Entity entity4 = new Serialzable2Entity();
        entity4.initListValue();
        Serialzable2Entity clone4 ;
        stopWatch.start(" 流深拷贝1");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(entity4);
        oos.flush();
        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        stopWatch.stop();
        stopWatch.start(" 流深拷贝2");
        clone4 = (Serialzable2Entity) ois.readObject();
        stopWatch.stop();

        System.out.println(stopWatch.prettyPrint());
    }
}
