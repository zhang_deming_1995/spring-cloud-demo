package com.dm.cloud.testclass.deepclone;

import lombok.Data;

@Data
public class NormalPointerEntity{
    private String uuid;

    public NormalPointerEntity(){
    }

    public NormalPointerEntity(String uuid){
        this.uuid = uuid;
    }
}