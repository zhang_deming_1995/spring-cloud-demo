package com.dm.cloud.testclass;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 制造OOM
 */
@Component
public class OOMCreater {

    private List<String> test=  new ArrayList<>();

    public void addUuidStr(){
        String str = UUID.randomUUID().toString();
        test.add(str);
    }
}