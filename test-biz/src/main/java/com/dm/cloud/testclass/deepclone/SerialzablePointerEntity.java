package com.dm.cloud.testclass.deepclone;

import lombok.Data;

import java.io.Serializable;

@Data
public class SerialzablePointerEntity implements Serializable {
    private String uuid;
    public SerialzablePointerEntity(){
    }

    public SerialzablePointerEntity(String uuid){
        this.uuid = uuid;
    }

}