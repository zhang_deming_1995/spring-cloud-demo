package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TestDao extends BaseMapper<Object> {

    @Select("SELECT TABLE_NAME AS tableName FROM information_schema.TABLES WHERE TABLE_SCHEMA = (SELECT DATABASE ())  ")
    List<String> getCurrentDbTable();

    @Insert({"insert into test (field_1,field_2,field_3,field_4,field_5)values(#{testValue},#{testValue},#{testValue},#{testValue},#{testValue})"})
    long insertTest(String testValue);

    @Insert({"insert into a (field_a)values(#{testValue})"})
    long insertA(String testValue);

    @Insert({"insert into b (field_b)values(#{testValue})"})
    long insertB(String testValue);
}
