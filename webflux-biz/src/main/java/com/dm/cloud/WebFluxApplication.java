package com.dm.cloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebFluxApplication {

    @Autowired
    public static void main(String[] args) {
        SpringApplication.run(WebFluxApplication.class, args);
    }
}