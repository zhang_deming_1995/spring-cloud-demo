package com.dm.cloud.common;

public class CloudConstant {
    /**
     * 头信息 用于fegin调用和网管调用时 不被拦截
     */
    public static final String GATEWAY_TOKEN_HEADER = "GATEWAY_TOKEN_HEADER";
    public static final String GATEWAY_TOKEN_VALUE = "DM_SPRINGCLOUD_GATEWAY_TOKEN_HEADER";
    public static final String GATEWAY_USERNAME = "GATEWAY_USERNAME";
    /**
     * 只允许程序内部调用的标志
     */
    public static final String AUTH_REQUEST_UUID = "e47b622056c941b7862df4ee800ba228";
}
