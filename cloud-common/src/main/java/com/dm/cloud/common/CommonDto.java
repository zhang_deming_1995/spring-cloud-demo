package com.dm.cloud.common;

import lombok.Data;

import java.util.Date;

@Data
public class CommonDto {
    /** 创建人 */
    private String createBy;
    /** 创建日期 */
    private Date createDate;
    /** 修改人 */
    private String updateBy;
    /** 修改日期 */
    private Date updateDate;

}
