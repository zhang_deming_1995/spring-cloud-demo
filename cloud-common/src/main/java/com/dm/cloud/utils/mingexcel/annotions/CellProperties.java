package com.dm.cloud.utils.mingexcel.annotions;

import com.dm.cloud.utils.mingexcel.annotions.innerannotation.Border;
import com.dm.cloud.utils.mingexcel.annotions.innerannotation.Font;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
* excel 单元格通用属性
*
*/
@Target({ElementType.FIELD,ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface CellProperties {

    /**
     * 前景色
     * 默认白色背景
     * @return
     */
    IndexedColors fillForegroundColor() default IndexedColors.WHITE;

    /**
     * 填充模式
     * @return
     */
    FillPatternType fillPattern() default FillPatternType.SOLID_FOREGROUND;

    /**
     * 水平分布样式
     * 默认左侧
     * @return
     */
    HorizontalAlignment horizontalAlignment() default HorizontalAlignment.LEFT;

    /**
     * 垂直分布样式
     * 默认顶部
     * @return
     */
    VerticalAlignment verticalAlignment() default VerticalAlignment.TOP;

    /**
     * 自动换行展示
     * 默认不自动换行
     * @return
     */
    boolean wrapText() default false;

    /**
     * 字体样式
     * @return
     */
    Font font() default @Font;

    /**
     * 边框样式
     * @return
     */
    Border border() default @Border;

}
