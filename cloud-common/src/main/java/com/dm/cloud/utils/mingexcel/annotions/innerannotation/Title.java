package com.dm.cloud.utils.mingexcel.annotions.innerannotation;

import com.dm.cloud.utils.mingexcel.annotions.CellProperties;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface Title {

    /**
     * 标题
     * @return
     */
    String title() default "";

    /**
     * 单元格设置
     * @return
     */
    CellProperties properties() default @CellProperties;

    /**
     * 行设置
     * @return
     */
    RowProperties row() default @RowProperties;
}
