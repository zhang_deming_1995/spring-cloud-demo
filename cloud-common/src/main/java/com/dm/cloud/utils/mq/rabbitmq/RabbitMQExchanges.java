package com.dm.cloud.utils.mq.rabbitmq;

public enum RabbitMQExchanges {
    EXCHANGE_A("my-mq-exchange_A"),
    EXCHANGE_B("my-mq-exchange_B"),
    EXCHANGE_C("my-mq-exchange_C");

    private String exchange;
    RabbitMQExchanges(String exchange){
        this.exchange = exchange;
    }

    public String getExchange(){
        return this.exchange;
    }
}
