package com.dm.cloud.utils.mq.rabbitmq;

public enum RabbitMQQueues {

    QUEUE_A("QUEUE_A"),
    QUEUE_B("QUEUE_B"),
    QUEUE_C("QUEUE_C");

    private String queue;
    RabbitMQQueues(String queue){
        this.queue = queue;
    }

    public String getQueue(){
        return this.queue;
    }
}
