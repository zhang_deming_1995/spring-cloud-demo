package com.dm.cloud.utils.mingexcel.annotions.innerannotation;

import com.dm.cloud.utils.mingexcel.annotions.CellProperties;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 表格合并设置
 */
@Target({ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface MergedColumnHeader {
    /**
     *  合并名称
     */
    String headerName() default "";

    /**
     * 从哪一列开始
     * @return
     */
    int from();

    /**
     * 至哪一列结束
     * @return
     */
    int to();

    /**
     * 至哪一列结束
     * @return
     */
    CellProperties properties() default @CellProperties;
}
