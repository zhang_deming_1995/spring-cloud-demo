package com.dm.cloud.utils.datetime;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateTimeTransfer {
    /***
     * Date 转 Calendar
     * @param date
     * @return
     */
    public static Calendar DateToCalendar(Date date){
        preCheck(date);
        Calendar calendar =Calendar.getInstance();
        calendar.setTime(date);

        return calendar;
    }

    /***
     * Calendar 转 Date
     * @param calendar
     * @return
     */
    public static Date CalendarToDate(Calendar calendar){
        preCheck(calendar);
        return calendar.getTime();
    }

    /***
     * LocalDateTime 转 Date
     * @param localDateTime
     * @return
     */
     public static Date LocaldatetimeToDate(LocalDateTime localDateTime){
         preCheck(localDateTime);
         return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
     }

    /***
     * Date 转 LocalDateTime
     * @param date
     * @return
     */
    public static LocalDateTime DateToLocalDatetime(Date date){
        preCheck(date);
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    /***
     * Date 转 时间戳
     * @param date
     * @return
     */
    public static long DateToTimestamp(Date date){
        preCheck(date);
        return date.getTime();
    }

    /***
     * 时间戳 转 Date
     * @param timestamp
     * @return
     */
    public static Date TimestampToDate(long timestamp){
        return new Date(timestamp);
    }

    /***
     * Date 转 LocalTime
     * @param date
     * @return
     */
    public static LocalTime DateToLocalTime(Date date){
        return DateToLocalDatetime(date).toLocalTime();
    }

    /**
     * 格式打印
     * @param object
     * @return
     */
    public static String print(Object object){
        return print(object,"yyyy-MM-dd HH:mm:ss");
    }

    public static String print(Object object,String format){
        preCheck(object);
        if(object instanceof java.util.Date ){
            return new SimpleDateFormat(format).format((Date)object);
        }else if(object instanceof java.time.LocalDateTime){
            return DateTimeFormatter.ofPattern(format).format((LocalDateTime)object);
        }else if(object instanceof Number){
            return new SimpleDateFormat(format).format(object);
        }else if(object instanceof java.util.Calendar){
            return new SimpleDateFormat(format).format(((Calendar)object).getTime());
        }else if(object instanceof java.time.LocalTime){
            LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(),(LocalTime)object);
            String formatTime = DateTimeFormatter.ofPattern(format).format(dateTime);
            return formatTime.substring(11);
        }else{
            throw new RuntimeException("Unsupported type:"+object.getClass().getName());
        }
    }

    /**
     * check
     * @param object
     */
    private static void preCheck(Object object){
        if(object == null){
            throw new RuntimeException("param cannot be null");
        }
    }

}
