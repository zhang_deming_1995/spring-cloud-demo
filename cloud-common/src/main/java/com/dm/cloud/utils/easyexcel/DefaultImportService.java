package com.dm.cloud.utils.easyexcel;

import java.util.List;

/**
 * @Author : zhangdeming
 * @Description: 导入服务接口
 * @Date : 2021-11-01
 * @Version : V0.0.1
 */
public interface DefaultImportService<T>{

    /**
     * 导入excel
     *
     * @param list 导入数据
     */
    void importExcel(List<T> list);

}
