package com.dm.cloud.utils.mq.rabbitmq;

public enum RabbitMQExchangeType {
    TOPIC,Direct,Fanout;
}
