package com.dm.cloud.utils.mingexcel.annotions;

import com.dm.cloud.utils.mingexcel.annotions.innerannotation.MergedHeaders;
import com.dm.cloud.utils.mingexcel.annotions.innerannotation.RowProperties;
import com.dm.cloud.utils.mingexcel.annotions.innerannotation.Title;
import com.dm.cloud.utils.mingexcel.enums.SameCellMergeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
* excel 单元格通用属性
*
*/
@Target({ElementType.TYPE})
@Retention(RUNTIME)
public @interface ExcelProperties {

    /**
     * 表格名称
     * @return
     */
    String sheetName();

    /**
     * 标题列表
     * @return
     */
    Title title();

    /**
     * 合并表头
     * @return
     */
    MergedHeaders[] mergedHeaders() default {};

    /**
     * 行设置
     * @return
     */
    RowProperties row() default @RowProperties;

    /**
     * 相同内容合并设置
     * @return
     */
    SameCellMergeEnum sameMerge() default SameCellMergeEnum.NULL_MERGE;
}
