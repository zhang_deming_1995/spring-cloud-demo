package com.dm.cloud.utils.minio;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "custom.minio")
public class MinioProperties {

    //    url
    private String url;

    //    是否启用证书 https
    private boolean secure;

    //    用户名
    private String accessKey;

    //    密码
    private String secureKey;
}
