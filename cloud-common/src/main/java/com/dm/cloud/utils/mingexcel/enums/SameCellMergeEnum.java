package com.dm.cloud.utils.mingexcel.enums;

public enum SameCellMergeEnum {
    NULL_MERGE,
    SAME_ROW_MERGE,//合并相同行
    SAME_ROW_MERGE_UNDIVIDE_FRONT//合并相同行 但是依照前一列的内容分割
}
