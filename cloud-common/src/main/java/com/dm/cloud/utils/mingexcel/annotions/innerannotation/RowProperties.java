package com.dm.cloud.utils.mingexcel.annotions.innerannotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
* excel 单元格通用属性
*
*/
@Target({ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface RowProperties {

    /**
     * 行高
     * @return
     */
    short height() default 400;
}
