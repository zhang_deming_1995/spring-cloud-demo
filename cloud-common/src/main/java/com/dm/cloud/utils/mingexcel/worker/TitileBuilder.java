package com.dm.cloud.utils.mingexcel.worker;

import com.dm.cloud.utils.mingexcel.annotions.innerannotation.Title;
import com.dm.cloud.utils.mingexcel.medium.Merger;
import lombok.Data;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@Data
public class TitileBuilder {
    //标题行信息
    private Title titles;

    public TitileBuilder(Title titles){
        this.titles = titles;
    }

    public void createTitle(XSSFWorkbook wb, XSSFSheet sheet, SheetWriterFrame sf){
        //存在标题则创建
        if(null != titles && !"".equals(titles.title())) {
            RowBuilder rowBuilder= new RowBuilder(titles.row());
            XSSFRow row = rowBuilder.createRow(sheet,sf.writedRows);
            Merger.createMergeCells(sf,wb,sheet,row,sf.writedRows,sf.writedRows,0,sf.allColumns-1,titles.properties(),titles.title());
            sf.writedRows++;
        }
    }

}
