package com.dm.cloud.utils;

import com.alibaba.fastjson.JSONObject;
import com.dm.cloud.common.R;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Collections;

@Slf4j
public class WebUtils {

    public static void writeJson(HttpServletResponse response, R resultVo) {

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");

        JSONObject res = new JSONObject();
        res.put("type",resultVo.getType());
        res.put("code",resultVo.getCode());
        res.put("result", Collections.EMPTY_LIST);
        res.put("message",resultVo.getMessage());

        try(PrintWriter out = response.getWriter()) {
            out.write(res.toString());
            out.flush();
        }catch (Exception ex){
            log.error("");
        }
    }

}
