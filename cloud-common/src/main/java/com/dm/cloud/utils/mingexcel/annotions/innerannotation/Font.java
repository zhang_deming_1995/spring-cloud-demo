package com.dm.cloud.utils.mingexcel.annotions.innerannotation;

import com.dm.cloud.utils.mingexcel.DefaultProperties;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTColor;

import java.awt.*;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface Font {
    /**
     * 字体
     * @return
     */
    String fontName() default DefaultProperties.FONT_NAME;

    /**
     * 字体颜色
     * @return
     */
    short color() default 0;

    /**
     * 是否加粗
     * @return
     */
    boolean bold() default DefaultProperties.FONT_BOLD;

    /**
     * 字体大小
     * @return
     */
    short height() default DefaultProperties.FONT_HEIGHT;
}
