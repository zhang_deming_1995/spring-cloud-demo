package com.dm.cloud.utils.mingexcel;

import org.apache.poi.ss.usermodel.IndexedColors;

/**
 * 一些默认属性
 */
public class DefaultProperties {
    //DEFAULT FONT
    public static final String FONT_NAME = "黑体";
    public static final boolean FONT_BOLD = false;
    public static final short FONT_HEIGHT = 200;
    public static final short FONT_COLOR = IndexedColors.BLACK.index;
}
