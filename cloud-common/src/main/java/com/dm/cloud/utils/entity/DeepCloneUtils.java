package com.dm.cloud.utils.entity;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Type;

/**
 * GSON方式对象深拷贝类
 */
@Log4j2
public class  DeepCloneUtils<T> {

    /**
     * 获取对象的深拷贝对象
     * @param source
     * @param <T>
     * @return
     */
    public static<T> T clone(T source){
        // 使用Gson序列化进行深拷贝
        Gson gson = new Gson();
        T clone = gson.fromJson(gson.toJson(source), (Type) source.getClass());
        return clone;
    }
}
