package com.dm.cloud.utils.security;


import org.springframework.util.Base64Utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;


/** 
 * RSA算法，实现数据的加密解密
 */
public class RSAUtil {
	private static Cipher cipher;  
    
    static{  
        try {  
            cipher = Cipher.getInstance("RSA");  
        } catch (NoSuchAlgorithmException e) {  
            e.printStackTrace();  
        } catch (NoSuchPaddingException e) {  
            e.printStackTrace();  
        }  
    }
    
    /** 
     * 生成密钥对 
     * @param filePath 生成密钥的路径 
     * @return 
     */  
    public static Map<String,String> generateKeyPair(String filePath){  
        try {  
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");  
            // 密钥位数  
            keyPairGen.initialize(2048);  
            // 密钥对  
            KeyPair keyPair = keyPairGen.generateKeyPair();  
            // 公钥  
            PublicKey publicKey = keyPair.getPublic();  
            // 私钥  
            PrivateKey privateKey = keyPair.getPrivate();  
            //得到公钥字符串  
            String publicKeyString = getKeyString(publicKey);  
            //得到私钥字符串  
            String privateKeyString = getKeyString(privateKey);  
            //将密钥对写入到文件  
            FileWriter pubfw = new FileWriter(filePath + "/rsa/publicKey.keystore");
            FileWriter prifw = new FileWriter(filePath + "/rsa/privateKey.keystore");
            BufferedWriter pubbw = new BufferedWriter(pubfw);  
            BufferedWriter pribw = new BufferedWriter(prifw);  
            pubbw.write(publicKeyString);  
            pribw.write(privateKeyString);  
            pubbw.flush();  
            pubbw.close();  
            pubfw.close();  
            pribw.flush();  
            pribw.close();  
            prifw.close();  
            //将生成的密钥对返回  
            Map<String,String> map = new HashMap<String,String>();  
            map.put("publicKey", publicKeyString);  
            map.put("privateKey", privateKeyString);  
            return map;  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return null;  
    }
    
    /** 
     * 得到公钥
     * @param key 密钥字符串（经过base64编码）
     * @throws Exception 
     */  
    public static PublicKey getPublicKey(String key) throws Exception {  
        byte[] keyBytes;  
        keyBytes = (Base64.getMimeDecoder()).decode(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);  
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");  
        PublicKey publicKey = keyFactory.generatePublic(keySpec);  
        return publicKey;  
    }  
      
    /** 
     * 得到私钥
     * @param key 密钥字符串（经过base64编码）
     * @throws Exception 
     */  
    public static PrivateKey getPrivateKey(String key) throws Exception {  
        byte[] keyBytes;  
        keyBytes = (Base64.getMimeDecoder()).decode(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);  
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");  
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);  
        return privateKey;
    }
    
    /** 
     * 得到密钥字符串（经过base64编码）
     * @return 
     */  
    public static String getKeyString(Key key) throws Exception {  
        byte[] keyBytes = key.getEncoded();  
        String s = (Base64.getMimeEncoder()).encodeToString(keyBytes);
        return s;  
    }
    
    /** 
     * 使用公钥对明文进行加密，返回BASE64编码的字符串 
     * @param publicKey 
     * @param plainText 
     * @return 
     */  
    public static String encrypt(PublicKey publicKey, String plainText){  
        try {             
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);  
            byte[] enBytes = cipher.doFinal(plainText.getBytes());            
            return (Base64.getMimeEncoder()).encodeToString(enBytes);
        } catch (InvalidKeyException e) {  
            e.printStackTrace();  
        } catch (IllegalBlockSizeException e) {  
            e.printStackTrace();  
        } catch (BadPaddingException e) {  
            e.printStackTrace();  
        }  
        return null;  
    }  
      
    /** 
     * 使用keystore对明文进行加密 
     * @param publicKeystore 公钥文件路径 
     * @param plainText      明文 
     * @return 
     */  
    public static String fileEncrypt(String publicKeystore, String plainText){  
        try {             
            FileReader fr = new FileReader(publicKeystore);  
            BufferedReader br = new BufferedReader(fr);  
            String publicKeyString="";  
            String str;  
            while((str=br.readLine())!=null){  
                publicKeyString+=str;  
            }  
            br.close();  
            fr.close();  
            cipher.init(Cipher.ENCRYPT_MODE,getPublicKey(publicKeyString));  
            byte[] enBytes = cipher.doFinal(plainText.getBytes());            
            return (Base64.getMimeEncoder()).encodeToString(enBytes);
        } catch (InvalidKeyException e) {  
            e.printStackTrace();  
        } catch (IllegalBlockSizeException e) {  
            e.printStackTrace();  
        } catch (BadPaddingException e) {  
            e.printStackTrace();  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return null;  
    }
    
    /** 
     * 使用公钥对明文进行加密 
     * @param publicKey      公钥 
     * @param plainText      明文 
     * @return 
     */  
    public static String encrypt(String publicKey, String plainText){  
        try {              
            cipher.init(Cipher.ENCRYPT_MODE,getPublicKey(publicKey));  
            byte[] enBytes = cipher.doFinal(plainText.getBytes());            
            return (Base64.getMimeEncoder()).encodeToString(enBytes);
        } catch (InvalidKeyException e) {  
            e.printStackTrace();  
        } catch (IllegalBlockSizeException e) {  
            e.printStackTrace();  
        } catch (BadPaddingException e) {  
            e.printStackTrace();  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return null;  
    } 
      
    /** 
     * 使用私钥对明文密文进行解密 
     * @param privateKey 
     * @param enStr 
     * @return 
     */  
    public static String decrypt(PrivateKey privateKey, String enStr){  
        try {  
            cipher.init(Cipher.DECRYPT_MODE, privateKey);  
            byte[] deBytes = cipher.doFinal((Base64.getMimeDecoder()).decode(enStr));
            return new String(deBytes);  
        } catch (InvalidKeyException e) {  
            e.printStackTrace();  
        } catch (IllegalBlockSizeException e) {  
            e.printStackTrace();  
        } catch (BadPaddingException e) {  
            e.printStackTrace();  
        }
        return null;  
    }
    
    /** 
     * 使用私钥对密文进行解密 
     * @param privateKey       私钥 
     * @param enStr            密文 
     * @return 
     */  
    public static String decrypt(String privateKey, String enStr){  
        try {           
            cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(privateKey));  
            byte[] deBytes = cipher.doFinal((Base64.getMimeDecoder()).decode(enStr));
            return new String(deBytes);  
        } catch (InvalidKeyException e) {  
            e.printStackTrace();  
        } catch (IllegalBlockSizeException e) {  
            e.printStackTrace();  
        } catch (BadPaddingException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return null;  
    }
    
    /** 
     * 使用keystore对密文进行解密 
     * @param privateKeystore  私钥路径 
     * @param enStr            密文 
     * @return 
     */  
    public static String fileDecrypt(String privateKeystore, String enStr){  
        try {  
            FileReader fr = new FileReader(privateKeystore);  
            BufferedReader br = new BufferedReader(fr);  
            String privateKeyString="";  
            String str;  
            while((str=br.readLine())!=null){  
                privateKeyString+=str;  
            }  
            br.close();  
            fr.close();           
            cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(privateKeyString));  
            byte[] deBytes = cipher.doFinal((Base64.getMimeDecoder()).decode(enStr));
            return new String(deBytes);  
        } catch (InvalidKeyException e) {  
            e.printStackTrace();  
        } catch (IllegalBlockSizeException e) {  
            e.printStackTrace();  
        } catch (BadPaddingException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return null;  
    }

    public static final String SIGN_ALGORITHMS = "QDEICC_CNAEC_CFCA";
    // 生成密钥对
    public static KeyPair getKeyPair() throws Exception {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        KeyPair keyPair = keyGen.generateKeyPair();
        return keyPair;
    }

    // 用md5生成内容摘要，再用RSA的私钥加密，进而生成数字签名
    public static String getMd5Sign(String content , PrivateKey privateKey) throws Exception {
        byte[] contentBytes = content.getBytes("utf-8");
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initSign(privateKey);
        signature.update(contentBytes);
        byte[] signs = signature.sign();
        return (Base64.getMimeEncoder()).encodeToString(signs);
    }

    // 对用md5和RSA私钥生成的数字签名进行验证
    public static boolean verifyWhenMd5Sign(String content, String sign, PublicKey publicKey) throws Exception {
        byte[] contentBytes = content.getBytes("utf-8");
        Signature signature = Signature.getInstance("MD5withRSA");
        signature.initVerify(publicKey);
        signature.update(contentBytes);
        return signature.verify((Base64.getMimeDecoder()).decode(sign));
    }

    // 用sha1生成内容摘要，再用RSA的私钥加密，进而生成数字签名
    public static String getSha1Sign(String content , PrivateKey privateKey) throws Exception {
        byte[] contentBytes = content.getBytes("utf-8");
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initSign(privateKey);
        signature.update(contentBytes);
        byte[] signs = signature.sign();
        return (Base64.getMimeEncoder()).encodeToString(signs);
    }

    // 对用sha1和RSA私钥生成的数字签名进行验证
    public static boolean verifyWhenSha1Sign(String content, String sign, PublicKey publicKey) throws Exception {
        byte[] contentBytes = content.getBytes("utf-8");
        Signature signature = Signature.getInstance("SHA1withRSA");
        signature.initVerify(publicKey);
        signature.update(contentBytes);
        return signature.verify((Base64.getMimeDecoder()).decode(sign));
    }

    /**
     * RSA签名
     * @param content 待签名数据
     * @param privateKey 商户私钥
     * @param input_charset 编码格式
     * @return 签名值
     */
    public static String sign(String content, String privateKey, String input_charset) {
        try {
            PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec((Base64.getMimeDecoder()).decode(privateKey));
            KeyFactory keyf = KeyFactory.getInstance("RSA");
            PrivateKey priKey = keyf.generatePrivate(priPKCS8);
            Signature signature = Signature.getInstance(SIGN_ALGORITHMS);
            signature.initSign(priKey);
            signature.update( content.getBytes(input_charset) );

            byte[] signed = signature.sign();
            return (Base64.getMimeEncoder()).encodeToString(signed);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static final String KEY_ALGORITHM = "RSA";
    /**
     * RSA最大加密明文最大大小
     */
    public static final int MAX_ENCRYPT_BLOCK = 117;
    /**
     * RSA解密字节长度
     */
    public static final int DECRIPYT_BYTE_LEN = 256;

    /**
     * base64解码
     * @param content
     * @return
     */
    public static byte[] decryptBASE64(String content) {
        return Base64Utils.decode(content.getBytes());
    }

    /**
     * base64编码
     * @param bytes
     * @return
     */
    public static String encryptBASE64(byte[] bytes) {
        return new String(Base64Utils.encode(bytes));
    }

    /**
     * 通过私钥解密
     * base64解密 -》 rsa解密
     * @param data
     * @param privateKey
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPrivateKey(byte[] data, String privateKey)
            throws Exception {
        // 对密钥解密
        byte[] keyBytes = decryptBASE64(privateKey);
        // 取得私钥
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PrivateKey privatizationKey = keyFactory.generatePrivate(pkcs8KeySpec);
        // 对数据解密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privatizationKey);
        byte[] resulybyte = cipher.doFinal(data);
        return resulybyte;
    }

    /**
     * 私钥分段解密
     * @param content
     * @param privateKey
     * @return
     */
    public static String decryptByShort (String content, String privateKey) {
        String resultContent = "";
        try {
            byte[] resultbyte = new byte[0];

            if (content != null && content.trim().length() > 0) {
                byte[] cipherbytes = Base64Utils.decodeFromString(content);
                int rounds = cipherbytes.length / DECRIPYT_BYTE_LEN;
                int modlen = cipherbytes.length % DECRIPYT_BYTE_LEN;
                if(modlen > 0){
                    rounds += 1;
                }
                for (int i = 0; i < rounds; i++) {
                    byte[] itemBytes;
                    if(i<rounds-1) {
                        itemBytes = new byte[DECRIPYT_BYTE_LEN];
                        System.arraycopy(cipherbytes, i * DECRIPYT_BYTE_LEN, itemBytes, 0, DECRIPYT_BYTE_LEN);
                    }else{
                        int len = modlen==0?DECRIPYT_BYTE_LEN:modlen;
                        itemBytes = new byte[len];
                        System.arraycopy(cipherbytes, i * DECRIPYT_BYTE_LEN, itemBytes, 0, len);
                    }
                    try {
                        byte[] itemResultBytes = decryptByPrivateKey(itemBytes, privateKey);
                        byte[] resultbyte2 = new byte[resultbyte.length + itemResultBytes.length];
                        System.arraycopy(resultbyte, 0, resultbyte2, 0, resultbyte.length);
                        System.arraycopy(itemResultBytes, 0, resultbyte2, resultbyte.length, itemResultBytes.length);
                        resultbyte = resultbyte2;
                    } catch (Exception e) {
                        System.out.println(e.getLocalizedMessage());
                    }
                }
            }
            resultContent = new String(resultbyte);
        } catch (Exception e) {
            throw new RuntimeException("RSA解密失败");
        }
        return resultContent;
    }

    /**
     * 通过公钥加密
     * rsa加密 -》 base64加密
     * @param data
     * @param var1
     * @param var2
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPublicKey(
            byte[] data, int var1, int var2, String key)
            throws Exception {
        // 对公钥解密
        byte[] keyBytes = decryptBASE64(key);
        // 取得公钥
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PublicKey publicKey = keyFactory.generatePublic(x509KeySpec);
        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(data, var1, var2);
    }

    /**
     * 公钥分段加密
     * @param content
     * @param publicKey
     * @return
     */
    public static String encryptByShort(String content, String publicKey) {
        String resultContent = "";
        try {
            //base64加密
            byte[] inputBytes = content.getBytes();
            int inputLenth = inputBytes.length;
            // 标识
            int offSet = 0;
            StringBuffer buffer = new StringBuffer();
            byte[] cache ;
            while (inputLenth - offSet > 0) {
                //超出最大字节数分组加密
                if (inputLenth - offSet > MAX_ENCRYPT_BLOCK) {
                    cache = encryptByPublicKey(inputBytes, offSet, MAX_ENCRYPT_BLOCK, publicKey);
                    offSet += MAX_ENCRYPT_BLOCK;
                } else {
                    //直接加密
                    cache = encryptByPublicKey(inputBytes, offSet, inputLenth - offSet, publicKey);
                    offSet = inputLenth;
                }
                buffer.append(cache);
            }
            resultContent = buffer.toString();
        } catch (InvalidKeySpecException e) {
//            System.err.println("encryptByShort加密出错:" + e.getMessage() + ":" + "加密内容:" + content);
            throw new RuntimeException("无效公钥");
        }catch (Exception e) {
//            System.err.println("encryptByShort加密出错:" + e.getMessage() + ":" + "加密内容:" + content);
            throw new RuntimeException("RSA加密失败");
        }
        return resultContent;
    }
}
