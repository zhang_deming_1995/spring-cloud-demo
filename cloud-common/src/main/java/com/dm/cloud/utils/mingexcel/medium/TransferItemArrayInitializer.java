package com.dm.cloud.utils.mingexcel.medium;

import com.dm.cloud.utils.mingexcel.entity.TransferItem;

public interface TransferItemArrayInitializer {

    /**
     * 初始化转换列表
     * @return
     */
    TransferItem[] initItems();
}
