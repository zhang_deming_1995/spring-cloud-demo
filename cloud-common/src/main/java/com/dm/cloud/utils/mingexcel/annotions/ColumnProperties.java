package com.dm.cloud.utils.mingexcel.annotions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
* excel 单元格通用属性
*
*/
@Target({ElementType.FIELD})
@Retention(RUNTIME)
public @interface ColumnProperties {

    int index() default 0;

    /**
     *  表头
     */
    String headerName() default "";

    /**
     *  行相同时合并
     */
    String rowsSameMerge() default "";

    /**
     * 自动调整行宽
     * 默认不自动调整行宽
     * @return
     */
    boolean autoWidth() default false;
}
