package com.dm.cloud.utils.easyexcel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : zhangdeming
 * @Description: 默认导入监听抽象类
 * @Date : 2021-11-01
 * @Version : V0.0.1
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DefaultImportListener<T> extends AnalysisEventListener<T> {
    /**
     * 缓存的数据列表
     */
    private List<T> list = new ArrayList<>();

    private DefaultImportService<T> service;

    public DefaultImportListener(DefaultImportService service) {
        this.service = service;
    }

    @Override
    public void invoke(T excel, AnalysisContext analysisContext) {
        list.add(excel);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 调用importer方法
        service.importExcel(list);
        // 存储完成清理list
        list.clear();
    }
}
