package com.dm.cloud.utils.mingexcel.worker;

import com.dm.cloud.utils.mingexcel.annotions.ColumnProperties;
import com.dm.cloud.utils.mingexcel.annotions.DropdownProperties;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class SheetReader extends SheetReaderFrame {

    public SheetReader(Class<?> clazz) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        super(clazz);
    }

    /**
     * 导入excel
     * @param multipartFile
     * @param sheetIndex
     * @return
     * @throws IOException
     * @throws InstantiationException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public <T> List<T> importExcel(MultipartFile multipartFile,int sheetIndex) throws Exception {

        InputStream inputStream = multipartFile.getInputStream();
        String fileName = multipartFile.getOriginalFilename();

        Workbook workbook = null;
        if (fileName.endsWith(".xls")) {
            workbook = new HSSFWorkbook(inputStream);
        } else if (fileName.endsWith(".xlsx")) {
            workbook = new XSSFWorkbook(inputStream);
        }else{
            workbook = new HSSFWorkbook(inputStream);
        }

        //获取文件中的表格
        Sheet sheetAt = workbook.getSheetAt(sheetIndex);
        //获取文件中的长度从0开始
        int lastRowNum = sheetAt.getLastRowNum();
        List<T> list = new ArrayList<>();

        DataFormatter formatter = new DataFormatter();
        //从1开始，0为表头
        for (int i = this.contentStartRow; i <= lastRowNum; i++) {
            Row row = sheetAt.getRow(i);
            Object t = clazz.getDeclaredConstructor().newInstance();
            Field[] tFields = clazz.getDeclaredFields();
            for (int j = 0; j < tFields.length;j++) {
                Field f = tFields[j];
                f.setAccessible(true);
                //单元格属性
                ColumnProperties columnProperties = f.getAnnotation(ColumnProperties.class);
                //下拉框属性
                DropdownProperties dropdownProperties = f.getAnnotation(DropdownProperties.class);
                if(null!=dropdownProperties){
                    f.set(t, transferWorker.transferFromExcel(Integer.valueOf(columnProperties.index()),formatter.formatCellValue(row.getCell(columnProperties.index()))));
                }else{
                    f.set(t, formatter.formatCellValue(row.getCell(columnProperties.index())));
                }
            }
            list.add((T)t);
        }
        return list;
    }
}
