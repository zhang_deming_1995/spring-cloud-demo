package com.dm.cloud.utils.mq.rabbitmq;

public enum RabbitMQRoutingKeys {

    ROUTINGKEY_A("spring-boot-routingKey_A"),
    ROUTINGKEY_A2("spring-boot-routingKey_A2"),
    ROUTINGKEY_B("*.test2.*");

    private String routingKey;

    RabbitMQRoutingKeys(String routingKey) {
        this.routingKey = routingKey;
    }

    public String getRoutingKey() {
        return this.routingKey;
    }
}
