package com.dm.cloud.utils.mingexcel.worker;

import com.dm.cloud.utils.mingexcel.annotions.CellProperties;
import com.dm.cloud.utils.mingexcel.annotions.ColumnProperties;
import com.dm.cloud.utils.mingexcel.annotions.DropdownProperties;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.xssf.usermodel.*;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class SheetWriter extends SheetWriterFrame {


    public SheetWriter(Class<?> clazz, List<?> data) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        super(clazz, data);
    }

    /**
     * 初始化表格
     * @param wb
     */
    XSSFSheet createSheet(XSSFWorkbook wb) throws IllegalAccessException {
        //创建工作单元
        XSSFSheet sheet = wb.createSheet(sheetName);
        //不显示网格线
//        sheet.setDisplayGridlines(false);
        //创建标题
        titileBuilder.createTitle(wb,sheet,this);
        //创建合并行
        mergedHeadersBuilder.createMergedHeaders(wb,sheet,this);
        //创建表头
        this.createContent(wb,sheet,true,null);
        return sheet;
    };

    void createDataContent(XSSFWorkbook wb, XSSFSheet sheet) throws IllegalAccessException {
        if(CollectionUtils.isNotEmpty(this.data)){
            //填充内容
            for (int j = 0; j < this.data.size(); j++) {
                Object t = this.data.get(j);
                this.createContent(wb,sheet,false,t);
            }
        }
    }

    /**
     * 创建内容
     * @param wb
     * @param sheet
     * @param forHeader
     * @param object
     * @throws IllegalAccessException
     */
    void createContent(XSSFWorkbook wb, XSSFSheet sheet,boolean forHeader,Object object) throws IllegalAccessException {
        XSSFRow headerRow = this.rowBuilder.createRow(sheet,this.writedRows);
        Field[] tFields = clazz.getDeclaredFields();
        for (int i = 0; i < tFields.length; i++) {
            Field f = tFields[i];
            f.setAccessible(true);
            //单元格属性
            ColumnProperties columnProperties = f.getAnnotation(ColumnProperties.class);
            CellProperties cellProperties = f.getAnnotation(CellProperties.class);
            if(null != columnProperties){

                //单元格位置
                int index = columnProperties.index();
                XSSFCell cell;
                cell = headerRow.createCell(index);
                if(forHeader) {
                    //表头
                    cell.setCellValue(new XSSFRichTextString(columnProperties.headerName()));
                    //判断是不是下拉框
                    String[] dropResult = transferWorker.getValueArray(Integer.valueOf(index));
                    if (null != dropResult && dropResult.length > 0) {
                        this.selectList(sheet, this.writedRows + 1, 65535, index, index, dropResult);
                    }
                }else{
                    //需要导出的内容
                    DropdownProperties dropdownProperties = f.getAnnotation(DropdownProperties.class);
                    if(null!=dropdownProperties){
                        cell.setCellValue(null == f.get(object)?"": transferWorker.transferToExcel(false,Integer.valueOf(index),String.valueOf(f.get(object))));
                    }else{
                        cell.setCellValue(null == f.get(object)?"": String.valueOf(f.get(object)));
                    }
                }
                XSSFCellStyle cellstyle = this.propertiesInitializer.createCellStyle(wb, cellProperties);
                cell.setCellStyle(cellstyle);
            }
        }
        this.writedRows++;
    }
}
