package com.dm.cloud.utils.mingexcel.worker;

import com.dm.cloud.utils.mingexcel.annotions.innerannotation.MergedColumnHeader;
import com.dm.cloud.utils.mingexcel.annotions.innerannotation.MergedHeaders;
import com.dm.cloud.utils.mingexcel.medium.Merger;
import lombok.Data;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@Data
public class MergedHeadersBuilder {
    //标题行信息
    private MergedHeaders[] mergedHeaders;

    public MergedHeadersBuilder(MergedHeaders[] mergedHeaders){
        this.mergedHeaders = mergedHeaders;
    }

    public void createMergedHeaders(XSSFWorkbook wb, XSSFSheet sheet, SheetWriterFrame sf){
        //存在标题则创建
        if(null != mergedHeaders && mergedHeaders.length>0) {
            for (MergedHeaders mergedHeader : mergedHeaders) {
                RowBuilder rowBuilder= new RowBuilder(mergedHeader.row());
                XSSFRow row = rowBuilder.createRow(sheet,sf.writedRows);
                if(null!= mergedHeader.merge() && mergedHeader.merge().length>0){
                    for (MergedColumnHeader mergedColumnHeader : mergedHeader.merge()) {
                        Merger.createMergeCells(sf,wb,sheet,row,sf.writedRows,sf.writedRows,mergedColumnHeader.from(),mergedColumnHeader.to(),mergedColumnHeader.properties(),mergedColumnHeader.headerName());
                    }
                }
                sf.writedRows++;
            }
        }
    }
}
