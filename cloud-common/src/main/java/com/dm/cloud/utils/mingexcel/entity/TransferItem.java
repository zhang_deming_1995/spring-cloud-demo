package com.dm.cloud.utils.mingexcel.entity;

import lombok.Data;

@Data
public class TransferItem {

    public TransferItem(){ }

    public TransferItem(Object key,String value){
        this.key = key;
        this.value = value;
    }

    /**
     * 系统内部值
     */
    private Object key;

    /**
     * 对外展示值
     */
    private String value;

}
