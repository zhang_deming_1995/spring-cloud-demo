package com.dm.cloud.utils.mq.rabbitmq;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "custom.mq.rabbitmq")
public class RabbitProperties {

    public String host;
    public int port;
    public String username;
    public String password;
}
