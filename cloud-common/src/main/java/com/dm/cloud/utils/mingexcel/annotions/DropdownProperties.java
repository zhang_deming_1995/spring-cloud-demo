package com.dm.cloud.utils.mingexcel.annotions;

import com.dm.cloud.utils.mingexcel.medium.TransferItemArrayInitializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 设置单元格下拉框属性
 */
@Target({ElementType.FIELD})
@Retention(RUNTIME)
public @interface DropdownProperties {
    /**
     * 类名
     * @return
     */
    Class<? extends TransferItemArrayInitializer> clazz();
}
