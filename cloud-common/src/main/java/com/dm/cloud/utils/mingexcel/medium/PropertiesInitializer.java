package com.dm.cloud.utils.mingexcel.medium;

import com.dm.cloud.utils.mingexcel.DefaultProperties;
import com.dm.cloud.utils.mingexcel.annotions.CellProperties;
import com.dm.cloud.utils.mingexcel.annotions.innerannotation.Border;
import com.dm.cloud.utils.mingexcel.annotions.innerannotation.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.HashMap;
import java.util.Map;

public class PropertiesInitializer {
    ThreadLocal<Map<String,XSSFCellStyle>> styleList = new ThreadLocal<>();

    /**
     * 创建字体格式
     * @param wb
     * @param font
     */
    public XSSFFont createFont(XSSFWorkbook wb,Font font){
        XSSFFont fontStyle = wb.createFont();

        if(null == font){
            fontStyle.setBold(DefaultProperties.FONT_BOLD);
            fontStyle.setFontName(DefaultProperties.FONT_NAME);
            fontStyle.setFontHeight(DefaultProperties.FONT_HEIGHT);
            fontStyle.setColor(DefaultProperties.FONT_COLOR);
        }else{
            fontStyle.setBold(font.bold());
            fontStyle.setFontName(font.fontName());
            fontStyle.setFontHeight(font.height());
            fontStyle.setColor(font.color());
        }

        return fontStyle;
    }

    /**
     * 合并单元格得边框样式
     * @param sheet
     * @param subRegion
     * @param border
     */
    public void createMergedBorder(XSSFSheet sheet, CellRangeAddress subRegion, Border border){
        if(null !=border && border.showBorder()){
            RegionUtil.setBorderBottom(border.bottom(), subRegion, sheet);
            RegionUtil.setBorderLeft(border.left(), subRegion, sheet);
            RegionUtil.setBorderTop(border.top(), subRegion, sheet);
            RegionUtil.setBorderRight(border.right(), subRegion, sheet);
            RegionUtil.setBottomBorderColor(border.bottomColor().index, subRegion, sheet);
            RegionUtil.setLeftBorderColor(border.leftColor().index, subRegion, sheet);
            RegionUtil.setTopBorderColor(border.topColor().index, subRegion, sheet);
            RegionUtil.setRightBorderColor(border.rightColor().index, subRegion, sheet);
        }
    }

    /**
     * 设置单元格边框样式
     * @param cellStyle
     * @param border
     */
    public void createBorder(XSSFCellStyle cellStyle, Border border){
        if(null !=border && border.showBorder()){
            cellStyle.setBorderTop(border.top());//上边框
            cellStyle.setBorderBottom(border.bottom()); //下边框
            cellStyle.setBorderLeft(border.left());//左边框
            cellStyle.setBorderRight(border.right());//右边框
            cellStyle.setTopBorderColor(border.topColor().getIndex());//上边框颜色
            cellStyle.setBottomBorderColor(border.bottomColor().getIndex()); //下边框颜色
            cellStyle.setLeftBorderColor(border.leftColor().getIndex());//左边框颜色
            cellStyle.setRightBorderColor(border.rightColor().getIndex());//右边框颜色
        }
    }

    /**
     * 创建单元格样式
     * @param wb
     * @param cellProperties
     * @return
     */
    public XSSFCellStyle createCellStyle(XSSFWorkbook wb, CellProperties cellProperties){
        XSSFCellStyle cellStyle = null;
        if(null != cellProperties){
            String key = Integer.toHexString(cellProperties.hashCode());
            if(null != styleList.get() && styleList.get().containsKey(key)){
                cellStyle = styleList.get().get(key);
            }else{
                cellStyle = wb.createCellStyle();
                cellStyle.setFillForegroundColor(cellProperties.fillForegroundColor().getIndex());
                cellStyle.setFillPattern(cellProperties.fillPattern());
                cellStyle.setAlignment(cellProperties.horizontalAlignment());
                cellStyle.setVerticalAlignment(cellProperties.verticalAlignment());
                cellStyle.setWrapText(cellProperties.wrapText());
                cellStyle.setFont(createFont(wb,cellProperties.font()));
                this.createBorder(cellStyle,cellProperties.border());

                Map<String,XSSFCellStyle> map = styleList.get();
                if(null == map){
                    map = new HashMap<>();
                }
                map.put(key,cellStyle);
                styleList.set(map);
            }
        }
        if(null == cellStyle){
            cellStyle = wb.createCellStyle();
        }
        return cellStyle;
    }

    /**
     * 创建单元格样式
     * @param wb
     * @param cellProperties
     * @return
     */
    public void setCellStyle(XSSFWorkbook wb, XSSFCellStyle cellStyle,CellProperties cellProperties){
        cellStyle.setFillForegroundColor(cellProperties.fillForegroundColor().getIndex());
        cellStyle.setFillPattern(cellProperties.fillPattern());
        cellStyle.setAlignment(cellProperties.horizontalAlignment());
        cellStyle.setVerticalAlignment(cellProperties.verticalAlignment());
        cellStyle.setWrapText(cellProperties.wrapText());
        cellStyle.setFont(createFont(wb,cellProperties.font()));
        this.createBorder(cellStyle,cellProperties.border());
    }

}
