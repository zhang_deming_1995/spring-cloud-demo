package com.dm.cloud.utils.mingexcel.annotions.innerannotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 表格合并设置
 */
@Target({ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface MergedHeaders {
    /**
     * 行设置
     * @return
     */
    RowProperties row() default @RowProperties;
    /**
     * 合并详情
     * @return
     */
    MergedColumnHeader[] merge() default {};
}
