package com.dm.cloud.utils.minio;

import lombok.Data;

import java.io.Serializable;
@Data
public class UpDownloadEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 上传地址
     */
    private String uploadUri;
    /**
     * 下载地址
     */
    private String downloadUri;
}
