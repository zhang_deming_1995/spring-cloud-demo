package com.dm.cloud.utils.pager;

import com.alibaba.excel.annotation.ExcelIgnore;
import lombok.Data;

@Data
public class PageVo {

    //页面
    @ExcelIgnore
    private Integer page;
    //页面大小
    @ExcelIgnore
    private Integer pageSize;
    //排序字段
    @ExcelIgnore
    private String field;
    //正序？倒序？
    @ExcelIgnore
    private String order;
}
