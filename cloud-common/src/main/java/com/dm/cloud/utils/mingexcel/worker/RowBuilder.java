package com.dm.cloud.utils.mingexcel.worker;

import com.dm.cloud.utils.mingexcel.annotions.innerannotation.RowProperties;
import lombok.Data;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

@Data
public class RowBuilder {
    //标题行信息
    private RowProperties rowProperties;

    public RowBuilder(RowProperties rowProperties){
        this.rowProperties = rowProperties;
    }

    public XSSFRow createRow(XSSFSheet sheet, int index){
        XSSFRow row = sheet.createRow(index);
        if(null!=rowProperties){
            row.setHeight(rowProperties.height());
        }
        return row;
    }
}
