package com.dm.cloud.utils.mingexcel.annotions.innerannotation;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.IndexedColors;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.ANNOTATION_TYPE})
@Retention(RUNTIME)
public @interface Border {
    /**
     * 是否显示边框
     * @return
     */
    boolean showBorder() default false;

    /**
     * 左边界
     * @return
     */
    BorderStyle left() default BorderStyle.THIN;

    /**
     * 右边界
     * @return
     */
    BorderStyle right() default BorderStyle.THIN;

    /**
     * 上边界
     * @return
     */
    BorderStyle top() default BorderStyle.THIN;

    /**
     * 下边界
     * @return
     */
    BorderStyle bottom() default BorderStyle.THIN;

    /**
     * 颜色边界(左)
     * @return
     */
    IndexedColors leftColor() default IndexedColors.BLACK;

    /**
     * 颜色边界(右)
     * @return
     */
    IndexedColors rightColor() default IndexedColors.BLACK;

    /**
     * 颜色边界(上)
     * @return
     */
    IndexedColors topColor() default IndexedColors.BLACK;

    /**
     * 颜色边界(下)
     * @return
     */
    IndexedColors bottomColor() default IndexedColors.BLACK;
}
