package com.dm.cloud.databases.mutimysql;

/**
 * 记录动态数据源
 * 由切片调用putDataSource方法，确认每次local中的值
 */
public class JdbcContextHolder {

    //本地线程共享对象
    private final static ThreadLocal<String> local = new ThreadLocal<>();

    public static void putDataSource(String name){
        local.set(name);
    }

    public static String getDataSource(){
        return local.get();
    }

    public static void removeDataSource(){
        local.remove();
    }
}