package com.dm.cloud.databases.mutimysql;

public interface DataBaseConst {
    String CLOUD_ALIBABA = "cloudalibaba";
    String CLOUD_AUTH = "cloudauth";
    String CLOUD_BASE = "cloudbase";
    String CLOUD_BUSINESS = "cloudbusiness";
}
