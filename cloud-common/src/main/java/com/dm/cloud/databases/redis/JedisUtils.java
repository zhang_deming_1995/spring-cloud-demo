package com.dm.cloud.databases.redis;

import com.dm.cloud.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.params.SetParams;

import java.util.List;
import java.util.Set;

@Slf4j
@Configuration
public class JedisUtils {

    //jedis配置
    static JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();

    static JedisPool jedisPool ;

    //初始化Jedis
    private static void initJedis(){
        if(null == jedisPool){

            //最带空闲数
            int maxIdle = 50;
            //最大连接数
            int maxTotal = 100;
            //最长等待时间
            int maxWaitMilis = 3000;

            String ip = "127.0.0.1";
            int port = 6379;

            //读取配置
            try {
                if(SpringContextUtils.containProperty("custom.datasource.redis.ip",true)){
                    ip = SpringContextUtils.getProperty("custom.datasource.redis.ip").toString();
                }else {
                    log.warn("cannot get the property [ custom.datasource.redis.ip ] for redis ip, using default ip [ 127.0.0.1 ] ");
                }

                if(SpringContextUtils.containProperty("custom.datasource.redis.port",true)){
                    port = (int) SpringContextUtils.getProperty("custom.datasource.redis.port");
                }else{
                    log.warn("cannot get the property [ custom.datasource.redis.port ] for redis port, using default port [ 6379 ] ");
                }
            }catch (Exception ex){
                log.error("initJedis error",ex.getMessage());
            }
            //设置最大空闲数
            jedisPoolConfig.setMaxIdle(maxIdle);
            //最大连接数
            jedisPoolConfig.setMaxTotal(maxTotal);
            //最大等待毫秒数
            jedisPoolConfig.setMaxWaitMillis(maxWaitMilis);

            jedisPool = new JedisPool(jedisPoolConfig,ip, port);
        }
    }

    /**
     * 获取符合条件的键
     * @param key  主键
     * @return
     */
    public static Set<String> keys(String pattern) {
        initJedis();
        try( Jedis jedis = jedisPool.getResource()){
            Set<String> obj= jedis.keys(pattern);
            return obj;
        }
    }

    /**
     * 获取值
     * @param key  主键
     * @return
     */
    public static Object getObject(String key) {
        initJedis();
        try( Jedis jedis = jedisPool.getResource()){
            Object obj= jedis.get(key);
            return obj;
        }
    }

    /**
     * 获取值
     * @param key  主键
     * @return
     */
    public static String getString(String key) {
        initJedis();
        try( Jedis jedis = jedisPool.getResource()){
            String obj= jedis.get(key);
            return obj;
        }
    }

    /**
     * 设置值
     * @param key  主键
     * @return
     */
    public static boolean setValue(String key,Object value) {
        initJedis();
        try( Jedis jedis = jedisPool.getResource()){
            String res = jedis.set(key, String.valueOf(value));
            if ("OK".equals(res)) {
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * 设置值
     * @param key  主键
     * @return
     */
    public static boolean setValue(String key,Object value,int expireTime) {
        initJedis();
        try( Jedis jedis = jedisPool.getResource()){

            //过期时长设
            SetParams params = SetParams.setParams().nx().px(expireTime);

            String res = jedis.set(key, String.valueOf(value),params);
            if ("OK".equals(res)) {
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * 设置值
     * @param key  主键
     * @return
     */
    public static void delValue(String key) {
        initJedis();
        try( Jedis jedis = jedisPool.getResource()){
            jedis.del(key);
        }
    }


    /**
     * 模糊查询批量删除
     */
    @SuppressWarnings("unchecked")
    public static long delWithPattern(String pattern) {
        initJedis();
        try(Jedis jedis = jedisPool.getResource()) {
            long delLength = 0;
            try {
                Set<String> keys = jedis.keys(pattern);
                if (keys.size() > 0) {
                    delLength = jedis.del(keys.toArray(new String[keys.size()]));
                }
            } catch (Exception e) {
                log.error("redis get keys time error：", e.getCause());
            }
            return delLength;
        }
    }


}
