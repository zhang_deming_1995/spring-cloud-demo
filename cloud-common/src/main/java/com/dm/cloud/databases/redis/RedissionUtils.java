package com.dm.cloud.databases.redis;

import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.RedissonMultiLock;
import org.redisson.RedissonRedLock;
import org.redisson.api.*;
import org.redisson.config.Config;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class RedissionUtils implements AutoCloseable {

    private RedissonClient redission;

    public RedissionUtils(String ip, String port) {
        redission = this.getRedisson(ip,port);
    }

    /**
     * 使用ip地址和端口创建Redisson
     *
     * @param ip
     * @param port
     * @return
     */
    private RedissonClient getRedisson(String ip, String port) {
        Config config = new Config();
        config.useSingleServer().setAddress(ip + ":" + port);
        RedissonClient redisson = Redisson.create(config);
        log.info("成功连接Redis Server" + "\t" + "连接" + ip + ":" + port + "服务器");
        return redisson;
    }

    /**
     * 关闭Redisson客户端连接
     *
     */
    private void closeRedisson() {
        redission.shutdown();
        log.info("成功关闭Redis Client连接");
    }

    /**
     * 获取字符串对象
     *
     * @param objectName
     * @return
     */
    public <T> RBucket<T> getRBucket(String objectName) {
        RBucket<T> bucket = redission.getBucket(objectName);
        return bucket;
    }

    /**
     * 获取Map对象
     *
     * @param objectName
     * @return
     */
    public <K, V> RMap<K, V> getRMap( String objectName) {
        RMap<K, V> map = redission.getMap(objectName);
        return map;
    }

    /**
     * 获取有序集合
     *
     * @param objectName
     * @return
     */
    public <V> RSortedSet<V> getRSortedSet(String objectName) {
        RSortedSet<V> sortedSet = redission.getSortedSet(objectName);
        return sortedSet;
    }

    /**
     * 获取集合
     *
     * @param objectName
     * @return
     */
    public <V> RSet<V> getRSet( String objectName) {
        RSet<V> rSet = redission.getSet(objectName);
        return rSet;
    }

    /**
     * 获取列表
     *
     * @param objectName
     * @return
     */
    public <V> RList<V> getRList( String objectName) {
        RList<V> rList = redission.getList(objectName);
        return rList;
    }

    /**
     * 获取队列
     *
     * @param objectName
     * @return
     */
    public <V> RQueue<V> getRQueue( String objectName) {
        RQueue<V> rQueue = redission.getQueue(objectName);
        return rQueue;
    }

    /**
     * 获取双端队列
     *
     * @param objectName
     * @return
     */
    public <V> RDeque<V> getRDeque( String objectName) {
        RDeque<V> rDeque = redission.getDeque(objectName);
        return rDeque;
    }

    /**
     * 获取阻塞队列
     *
     * @param objectName
     * @return
     */
     public <V> RBlockingQueue<V> getRBlockingQueue(String objectName)
     { RBlockingQueue rb=redission.getBlockingQueue(objectName); return rb; }

    /**
     * 获取原子数
     *
     * @param objectName
     * @return
     */
    public RAtomicLong getRAtomicLong( String objectName) {
        RAtomicLong rAtomicLong = redission.getAtomicLong(objectName);
        return rAtomicLong;
    }

    /**
     * 获取记数锁
     *
     * @param objectName
     * @return
     */
    public RCountDownLatch getRCountDownLatch(String objectName) {
        RCountDownLatch rCountDownLatch = redission
                .getCountDownLatch(objectName);
        return rCountDownLatch;
    }

    /**
     * 获取消息的Topic
     *
     * @param objectName
     * @return
     */
    public RTopic getRTopic( String objectName) {
        RTopic rTopic = redission.getTopic(objectName);
        return rTopic;
    }

    //***********************************************************************************
    // LOCK
    //***********************************************************************************
    /**
     * 可重入锁
     * 1、锁定十秒后自动解锁
     * lock.lock(10, TimeUnit.SECONDS);
     *
     * 2、尝试加锁 最多等待100秒 获得到锁之后10秒自动释放
     * boolean res = lock.tryLock(100, 10, TimeUnit.SECONDS);
     * if (res) {
     *    try {
     *      ...
     *    } finally {
     *        lock.unlock();
     *    }
     * }
     *
     * 3. 支持异步加锁lockAsync
     *
     * @param objectName
     * @return
     */
    public RLock getRLock( String objectName) {
        RLock rLock = redission.getLock(objectName);
        return rLock;
    }

    /**
     * 公平锁
     * @param objectName
     * @return
     */
    public RLock getFairLock( String objectName) {
        RLock fairLock = redission.getFairLock(objectName);
        return fairLock;
    }

    /**
     * 获取联锁
     * 可以是不同redis实例上的锁
     * 只有当所有的锁都获取到的时候能使真正的获取到锁
     * @param objectName
     * @return
     */
    public RedissonMultiLock getRedissonMultiLock( String... objectName) {

        List<RLock> rlockList =new ArrayList<>();

        if(objectName.length>0){
            for(String objectN : objectName){
                //单机实例实现联锁
                RLock lock = redission.getLock(objectN);
                rlockList.add(lock);
            }
        }else{
            throw new RuntimeException("请设置加锁元素");
        }

        RedissonMultiLock redissonMultiLock = new RedissonMultiLock(rlockList.toArray(new RLock[rlockList.size()]));

        return redissonMultiLock;
    }

    /**
     * 获取红锁 与联锁类似
     * 可以是不同redis实例上的锁
     * 当大部分锁都获取到的时候能使真正的获取到锁
     * @param objectName
     * @return
     */
    public RedissonRedLock getRedissonRedLock( String... objectName) {

        List<RLock> rlockList =new ArrayList<>();

        if(objectName.length>0){
            for(String objectN : objectName){
                //单机实例实现联锁
                RLock lock = redission.getLock("lock1");
                rlockList.add(lock);
            }
        }else{
            throw new RuntimeException("请设置加锁元素");
        }

        RedissonRedLock redissonRedLock = new RedissonRedLock(rlockList.toArray(new RLock[rlockList.size()]));

        return redissonRedLock;
    }

    /**
     * 读写锁
     * rwlock.readLock().lock();获取读锁
     * rwlock.writeLock().lock();获取写锁
     * 可设置自动解锁和尝试加锁
     * @param objectName
     * @return
     */
    public RReadWriteLock  getRReadWriteLock ( String objectName) {
        RReadWriteLock  rReadWriteLock  =redission.getReadWriteLock(objectName);
        return rReadWriteLock;
    }

    /**
     * 获取信号量
     * semaphore.acquire();
     * semaphore.release();
     * @param objectName
     * @return
     */
    public RSemaphore getRSemaphore ( String objectName) {
        RSemaphore  rSemaphore  =redission.getSemaphore(objectName);
        return rSemaphore;
    }

    @Override
    public void close() throws Exception {
        this.closeRedisson();
    }
}