package com.dm.cloud.databases.mutimysql;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "custom.datasource.muti.druid")
public class DruidProperties {

    private int initialSize;//启动程序时，在连接池中初始化多少个连接

    private int minIdle;//回收空闲连接时，将保证至少有minIdle个连接.

    private int maxActive;//连接池中最多支持多少个活动会话

    private int maxWait;//链接等待时长，连接超时，认为本次请求失败 单位毫秒，设置-1时表示无限等待

    private int timeBetweenEvictionRunsMillis;//检查空闲连接的频率，单位毫秒, 非正整数时表示不进行检查

    private int minEvictableIdleTimeMillis;//池中某个连接的空闲时长达到 timeBetweenEvictionRunsMillis 毫秒后, 连接池在下次检查空闲连接时，将回收该连接

    private boolean testWhileIdle;//当程序请求连接，池在分配连接时，是否先检查该连接是否有效。

    private boolean testOnBorrow;//程序申请连接时，检查连接有效性

    private boolean testOnReturn;//程序返还链接时，检查链接有效性

    private boolean poolPreparedStatements;

    private int maxPoolPreparedStatementPerConnectionSize;//每个连接最多缓存多少个SQL

    private String filters;//插件配置

    private String connectionProperties;//连接属性
}
