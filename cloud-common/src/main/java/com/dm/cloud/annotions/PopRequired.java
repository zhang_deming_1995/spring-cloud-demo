package com.dm.cloud.annotions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author : zhangdeming
 * @Description: 字段是否需要必填
 * @Date : 2021-10-28
 * @Version : V0.0.1
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface PopRequired {
    //报错信息
    String msg();
}