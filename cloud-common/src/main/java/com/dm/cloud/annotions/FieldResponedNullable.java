package com.dm.cloud.annotions;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author : zhangdeming
 * @Description: 响应时 标记类中的字段为null时是否返回
 * @Date : 2021-10-28
 * @Version : V0.0.1
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldResponedNullable {
    //针对哪个方法可以返回null
    String[] rtMethod() default {};
}