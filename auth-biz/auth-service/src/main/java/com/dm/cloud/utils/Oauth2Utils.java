package com.dm.cloud.utils;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Configuration
public class Oauth2Utils {

    private static UserDetailsService userDetailsService;
    private static DefaultTokenServices defaultTokenServices;
    private static TokenStore tokenStore;

    public Oauth2Utils(UserDetailsService userDetailsService, DefaultTokenServices defaultTokenServices, TokenStore tokenStore){
        this.userDetailsService = userDetailsService;
        this.defaultTokenServices = defaultTokenServices;
        this.tokenStore = tokenStore;
    }

    public static String getAuthorization(){
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        String authorization = request.getHeader("Authorization");
        return authorization;
    }

    public static String getUserNameByHeader(){
        String authorization = getAuthorization();
        return getByAuthorization(authorization).getUsername();
    }

    public static UserDetails getByAuthorization(String authorization){
        String token = getTokenStr(authorization);
        OAuth2Authentication auth2Authentication = defaultTokenServices.loadAuthentication(token);
        User u = (User)auth2Authentication.getUserAuthentication().getPrincipal();
        return userDetailsService.loadUserByUsername(u.getUsername());
    }

    public static boolean checkCurrentUser(String token,String username){
        UserDetails currentUser = getByAuthorization(token);
        return username.equals(currentUser.getUsername());
    }

    public static void revokeToken(String authorization){
        String token = getTokenStr(authorization);
        OAuth2AccessToken oAuth2AccessToken = tokenStore.readAccessToken(token);
//        OAuth2RefreshToken refreshToken = tokenStore.readRefreshToken(token);
        OAuth2RefreshToken refreshToken = oAuth2AccessToken.getRefreshToken();
        tokenStore.removeAccessToken(oAuth2AccessToken);
        tokenStore.removeRefreshToken(refreshToken);
    }

    /**
     * 从前端的信息中解析token信息
     * @param authorization
     * @return
     */
    private static String getTokenStr(String authorization){
        //多出一个空格
//        Bearer b56117fd-01d9-4479-8a6e-4a3bd0351c74
        authorization = authorization.substring(OAuth2AccessToken.BEARER_TYPE.length() +1).trim();

        int commaIndex = authorization.indexOf(',');
        if(commaIndex>0){
            authorization = authorization.substring(0,commaIndex);
        }
        return authorization;
    }
}
