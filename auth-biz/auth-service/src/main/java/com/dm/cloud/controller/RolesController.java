package com.dm.cloud.controller;

import com.dm.cloud.api.service.IRolesService;
import com.dm.cloud.api.vo.RolesVo;
import com.dm.cloud.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 角色
 */
@RestController
@RequestMapping("roles")
public class RolesController {

    @Autowired
    private IRolesService rolesService;

    /**
     * 添加角色
     * @return
     */
    @PostMapping("create")
    public R createRole(@RequestBody RolesVo roles){
        return rolesService.createRole(roles);
    }

    /**
     * 修改角色
     * @param roles
     * @return
     */
    @PostMapping("update")
    public R updateRole(@RequestBody RolesVo roles){
        return rolesService.updateRole(roles);
    }

    /**
     * 删除角色（逻辑删除）
     * @return
     */
    @PostMapping("delete")
    public R deleteRole(@RequestBody RolesVo roles){
        return rolesService.deleteRole(roles);
    }

    /**
     * 查询列表
     * @return
     */
    @PostMapping("page")
    public R selectPage(@RequestBody RolesVo roles){
        return rolesService.selectPage(roles);
    }


    /**
     * 根据角色获取资源
     * @return
     */
    @GetMapping("search/{roleId}")
    public R getRoleById(@PathVariable("roleId") String roleId){
        return rolesService.getRoleById(roleId);
    }

}