package com.dm.cloud.controller;

import com.dm.cloud.api.service.IResourcesService;
import com.dm.cloud.api.vo.ResourcesVo;
import com.dm.cloud.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 资源
 */
@RestController
@RequestMapping("resources")
public class ResourcesController {

    @Autowired
    private IResourcesService resourcesService;

    /**
     * 添加资源
     * @return
     */
    @PostMapping("create")
    public R createResources(@RequestBody ResourcesVo resourcesVo){
        return resourcesService.createResources(resourcesVo);
    }

    /**
     * 修改资源
     * @param resourcesVo
     * @return
     */
    @PostMapping("update")
    public R updateResources(@RequestBody ResourcesVo resourcesVo){
        return resourcesService.updateResources(resourcesVo);
    }

    /**
     * 删除资源
     * @return
     */
    @PostMapping("delete")
    public R deleteResources(@RequestBody ResourcesVo resourcesVo){
        return resourcesService.deleteResources(resourcesVo);
    }

    /**
     * 启用\禁用前的校验
     * @return
     */
    @PostMapping("checkInvalid")
    public R checkInvalid(@RequestBody ResourcesVo resourcesVo){
        return resourcesService.checkInvalid(resourcesVo);
    }

    /**
     * 启用\禁用
     * @return
     */
    @PostMapping("invalidResource")
    public R invalidResource(@RequestBody ResourcesVo resourcesVo) {
        return resourcesService.invalidResource(resourcesVo);
    }

    /**
     * 查询列表
     * @return
     */
    @PostMapping("page")
    public R selectPage(@RequestBody ResourcesVo resourcesVo){
        return resourcesService.selectPage(resourcesVo);
    }

    /**
     * 查询单条
     * @return
     */
    @GetMapping("search/{id}")
    public R searchResource(@PathVariable("id") String id){
        return resourcesService.searchResource(id);
    }

}