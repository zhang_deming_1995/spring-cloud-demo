package com.dm.cloud.utils;

public enum MysqlLoggerLevel {
    ERROR("error"),WARNING("warning"),INFO("info");

    private String level;

    MysqlLoggerLevel(String level){
        this.level = level;
    }

    public String get(){
        return this.level;
    }
}
