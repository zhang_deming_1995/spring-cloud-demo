package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.Resources;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ResourcesDao extends BaseMapper<Resources> {

    @Select("<script>" +
            " SELECT FROM ROLES A " +
            " LEFT JOIN ROLE_RESOURCE B ON A.ID = B.ROLE_ID " +
            " LEFT JOIN RESOURCES C ON B.RESOURCE_ID = C.ID " +
            " WHERE 1=1 " +
            " AND ${cond} = #{value} " +
            "</script>")
    List<Resources> getResourcesByRole(@Param("cond") String cond, @Param("value") String value);

    @Select("<script>" +
            " SELECT FROM USERS A" +
            " LEFT JOIN USER_ROLE B ON A.ID = B.USER_ID" +
            " LEFT JOIN ROLES C ON B.ROLE_ID = C.ID " +
            " LEFT JOIN ROLE_RESOURCE D ON C.ID = D.ROLE_ID " +
            " LEFT JOIN RESOURCES E ON D.RESOURCE_ID = E.ID " +
            " WHERE 1=1 " +
            " AND ${cond} = #{value} " +
            "</script>")
    List<Resources> getResourcesByUser(@Param("cond") String cond, @Param("value") String value);

    @Select("<script>" +
            " update t_s_resources set status = #{status} ,update_date = now() ,update_by = #{username} " +
            " where 1=1" +
            " and id in " +
            " <foreach collection=\"list\" item=\"item\" index=\"index\" open=\"(\" close=\")\" separator=\",\"> " +
            "     #{item}" +
            " </foreach>"+
            "</script>")
    void invalidByIds(@Param("list") List<String> list,@Param("username") String username,@Param("status") Integer status);
}
