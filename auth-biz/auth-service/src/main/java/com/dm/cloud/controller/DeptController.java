package com.dm.cloud.controller;

import com.dm.cloud.api.service.IDeptService;
import com.dm.cloud.api.vo.DepartmentVo;
import com.dm.cloud.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("dept")
public class DeptController {

    @Autowired
    private IDeptService deptService;

    /**
     * 创建部门
     * @return
     */
    @PostMapping("create")
    public R createDept(@RequestBody DepartmentVo deptVo){
        return deptService.createDept(deptVo);
    }

    /**
     * 修改部门
     * @param deptVo
     * @return
     */
    @PostMapping("update")
    public R updateDept(@RequestBody DepartmentVo deptVo){
        return deptService.updateDept(deptVo);
    }

    /**
     * 删除部门
     * @return
     */
    @PostMapping("delete")
    public R deleteDept(@RequestBody DepartmentVo deptVo){
        return deptService.deleteDept(deptVo);
    }

    /**
     * 启用\禁用前的
     * @return
     */
    @PostMapping("checkInvalid")
    public R checkInvalid(@RequestBody DepartmentVo deptVo){
        return deptService.checkInvalid(deptVo);
    }

    /**
     * 启用\禁用部门
     * @return
     */
    @PostMapping("invalidDept")
    public R invalidDept(@RequestBody DepartmentVo deptVo){
        return deptService.invalidDept(deptVo);
    }

    /**
     * 查询列表
     * @return
     */
    @PostMapping("page")
    public R selectPage(@RequestBody DepartmentVo deptVo){
        return deptService.selectPage(deptVo);
    }

    /**
     * 查询单条
     * @return
     */
    @GetMapping("search/{id}")
    public R searchDept(@PathVariable("id") String id){
        return deptService.searchDept(id);
    }

}