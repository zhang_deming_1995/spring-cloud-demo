package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.Roles;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RolesDao extends BaseMapper<Roles> {

    @Select("<script>" +
            " SELECT FROM USERS A " +
            " LEFT JOIN USER_ROLE B ON A.ID = B.USER_ID " +
            " WHERE 1=1 " +
            " AND ${cond} = #{value} " +
            "</script>")
    List<Roles> getRolesByUser(@Param("cond") String cond, @Param("value") String value);
}
