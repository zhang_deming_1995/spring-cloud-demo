package com.dm.cloud.controller;

import com.dm.cloud.api.dto.Users;
import com.dm.cloud.api.service.IUserAccountService;
import com.dm.cloud.api.vo.UserDetail;
import com.dm.cloud.api.vo.UserInfoVo;
import com.dm.cloud.api.vo.UserPublicMsg;
import com.dm.cloud.api.vo.UsersVo;
import com.dm.cloud.common.R;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user")
public class UsersController {

    @Autowired
    private IUserAccountService userAccountService;

    /**
     * 获取验证码 不需要通过权鉴验证
     * @return
     */
    @PostMapping("unauth/sms/create")
    public R smsCreate(@RequestBody String data) throws JsonProcessingException {
        JsonNode jsn = new ObjectMapper().readTree(data);
        String phoneNo = jsn.has("phoneNo")?jsn.get("phoneNo").asText():"";
        String type = jsn.has("type")?jsn.get("type").asText():"";
        return userAccountService.smsCreate(phoneNo,type);
    }

    /**
     * 登出账户
     * @return
     */
    @PostMapping("logout")
    public R logoutAccount(){
        return userAccountService.logoutAccount();
    }

    /**
     * 注册账号 个人去注册
     * @return
     */
    @PostMapping("unauth/create")
    public R unAuthCreateAccount(@RequestBody UsersVo users){
        return userAccountService.createAccount(users,0);
    }

    /**
     * 注册账号 管理员创建
     * 使用默认密码 !23
     * @return
     */
    @PostMapping("create")
    public R createAccount(@RequestBody UsersVo users){
        return userAccountService.createAccount(users,1);
    }

    /**
     * 注册账号 管理员创建
     * 使用默认密码 !23
     * @return
     */
    @PostMapping("self_create")
    public R selfCreateAccount(@RequestBody UsersVo users){
        return userAccountService.createAccount(users,0);
    }

    /**
     * 更新账户信息
     * @param users
     * @return
     */
    @PostMapping("update")
    public R updateAccount(@RequestBody UsersVo users){
        return userAccountService.updateAccount(users);
    }

    /**
     * 注销账户（逻辑删除）
     * @return
     */
    @PostMapping("delete")
    public R deleteAccount(@RequestBody UsersVo users){
        return userAccountService.deleteAccount(users);
    }

    /**
     * 查询列表
     * @return
     */
    @PostMapping("page")
    public R selectPage(@RequestBody UsersVo users){
        return userAccountService.selectPage(users);
    }

    /**
     * 查询列表
     * @return
     */
    @GetMapping("search/{id}")
    public R searchAccount(@PathVariable("id") String id){
        return userAccountService.searchAccount(id);
    }

    /**
     * 查询列表
     * @return
     */
    @GetMapping("getUserInfo")
    public R getUserInfo(){
        return userAccountService.getUserInfo();
    }

    /**
     * 更新账户信息
     * @param users
     * @return
     */
    @PostMapping("updateUserInfo")
    public R updateUserInfo(@RequestBody UserInfoVo userInfo){
        return userAccountService.updateUserInfo(userInfo);
    }

    /**
     * 获取当前用户的页面权限列表
     */
    @GetMapping("getMenuList")
    public R getMenuList(){
        return userAccountService.getMenuList();
    }

    /**
     * 只获有权限的权限标识
     */
    @GetMapping("getPermissions")
    public R getPermissions(){
        return userAccountService.getPermissions();
    }

    /**
     * 查询列表 内部使用
     * @return
     */
    @GetMapping("getCurrentUser/{uuid}")
    public R<Users> getCurrentUser(@PathVariable("uuid") String uuid){
        return userAccountService.getCurrentUser(uuid);
    }

    /**
     * 查询用户列表
     * @return
     */
    @GetMapping("getUserList")
    public R<List<UserDetail>> getUserList(){
        return userAccountService.getUserList();
    }

    /**
     * 查询用户列表
     * @return
     */
    @GetMapping("getUserPublicMsgById")
    public R<UserPublicMsg> getUserPublicMsgById(Long id){
        return userAccountService.getUserPublicMsgById(id);
    }

    /**
     * 获取头像上传下载地址
     */
    @GetMapping("/minio/avatar/api")
    public R minioAvatarApi(Long userId) {
        return userAccountService.minioAvatarApi(userId);
    }
}