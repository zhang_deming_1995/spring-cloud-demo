package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.RoleResource;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleResourceDao extends BaseMapper<RoleResource> {
    /**
     * 批量插入
     *
     * @return List<Integer>
     */
    @Insert("<script>" +
            "INSERT INTO t_s_role_resources " +
            "( resource_id,role_id ) " +
            "VALUES " +
            "<foreach collection=\"list\" index=\"index\" item=\"item\" separator=\",\">\n" +
            " ( #{item.resourceId},#{item.roleId} )" +
            "</foreach>" +
            "</script>")
    Integer batchInsert(@Param("list") List<RoleResource> list);
}
