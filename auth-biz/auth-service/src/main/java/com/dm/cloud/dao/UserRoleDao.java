package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.RoleResource;
import com.dm.cloud.api.dto.UserRole;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserRoleDao extends BaseMapper<UserRole> {

    @Insert("<script>" +
            "INSERT INTO t_s_role_user " +
            "( user_id,role_id ) " +
            "VALUES " +
            "<foreach collection=\"list\" index=\"index\" item=\"item\" separator=\",\">\n" +
            " ( #{item.userId},#{item.roleId} )" +
            "</foreach>" +
            "</script>")
    Integer batchInsert(@Param("list") List<UserRole> list);
}
