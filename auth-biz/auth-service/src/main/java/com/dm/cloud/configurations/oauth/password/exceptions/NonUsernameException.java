package com.dm.cloud.configurations.oauth.password.exceptions;


import org.springframework.security.core.AuthenticationException;

public class NonUsernameException extends AuthenticationException {

    public NonUsernameException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public NonUsernameException(String msg) {
        super(msg);
    }
}
