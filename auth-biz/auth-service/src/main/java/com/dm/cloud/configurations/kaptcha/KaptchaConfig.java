package com.dm.cloud.configurations.kaptcha;

import com.google.code.kaptcha.Producer;
import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
public class KaptchaConfig {

    @Bean
    public Producer kaptchaProducer() {

        Properties properties = new Properties();

        //边框样式
        properties.setProperty("kaptcha.border", "yes"); //是否边框
        properties.setProperty("kaptcha.border.color", "black"); //边框颜色

        //图片大小
        properties.setProperty("kaptcha.image.width", "100"); //图片长度
        properties.setProperty("kaptcha.image.height", "40"); //图片宽度

        //验证码内容
        properties.setProperty("kaptcha.textproducer.char.string", "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"); //验证码内容集合
        properties.setProperty("kaptcha.textproducer.char.length", "4"); //验证码长度
        properties.setProperty("kaptcha.textproducer.char.space", "2"); //验证码间隔
        properties.setProperty("kaptcha.textproducer.font.names", "微软雅黑"); //验证码字体
        properties.setProperty("kaptcha.textproducer.font.size", "32"); //验证码字体大小
        properties.setProperty("kaptcha.textproducer.font.color", "0,0,0"); //验证码字体颜色

        //干扰项
        properties.setProperty("kaptcha.noise.impl", "com.google.code.kaptcha.impl.NoNoise"); //实现类
        properties.setProperty("kaptcha.noise.color", "blue"); //干扰颜色

        //渲染效果
        //水纹 com.google.code.kaptcha.impl.WaterRipple
        //鱼眼 com.google.code.kaptcha.impl.FishEyeGimpy
        //阴影 com.google.code.kaptcha.impl.ShadowGimpy
        properties.setProperty("kaptcha.obscurificator.impl", "com.google.code.kaptcha.impl.ShadowGimpy");

        //背景图片渲染器
        properties.setProperty("kaptcha.background.impl", "com.google.code.kaptcha.impl.DefaultBackground");
        properties.setProperty("kaptcha.background.clear.from", "251,255,242");
        properties.setProperty("kaptcha.background.clear.to", "202,235,216");

        //文字渲染器
        properties.setProperty("kaptcha.word.impl", "com.google.code.kaptcha.text.impl.DefaultWordRenderer");

        //使用默认图片实现类
        DefaultKaptcha kaptcha = new DefaultKaptcha();
        Config config = new Config(properties);
        kaptcha.setConfig(config);
        return kaptcha;
    }

}