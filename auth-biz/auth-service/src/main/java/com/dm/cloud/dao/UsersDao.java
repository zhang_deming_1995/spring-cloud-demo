package com.dm.cloud.dao;

import com.alibaba.nacos.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.Resources;
import com.dm.cloud.api.dto.Users;
import com.dm.cloud.api.vo.UsersVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UsersDao extends BaseMapper<Users> {

    @Select("<script>" +
            " SELECT A.* FROM t_s_user A " +
            " LEFT JOIN t_s_department_user B ON A.ID = B.user_id " +
            " WHERE 1=1 " +
            " <if test=\"user.userName != null and user.userName !='' \">  " +
            "    <![CDATA[ and A.user_name like concat('%',#{user.userName},'%')  ]]>" +
            " </if>"+
            " <if test=\"user.realName != null and user.realName !='' \">  " +
            "    <![CDATA[ and A.real_name like concat('%',#{user.realName},'%')  ]]>" +
            " </if>"+
            " <if test=\"user.phoneNo != null and user.phoneNo !='' \">  " +
            "    <![CDATA[ and A.phone_no like concat('%',#{user.phoneNo},'%')  ]]>" +
            " </if>"+
            " <if test=\"user.departmentId != null \">  " +
            "    <![CDATA[ and B.department_id = #{user.departmentId}  ]]>" +
            " </if>" +
            " and A.delete_flag = 0" +
            " order by A.id desc "+
            "</script>")
    List<Users> selectListJoinDepartment(@Param("user")UsersVo userVo);

    @Select("<script>" +
            " SELECT distinct D.* FROM t_s_user A " +
            " LEFT JOIN t_s_role_user B ON A.ID = B.user_id" +
            " LEFT JOIN t_s_role_resources C ON B.role_id = C.role_id " +
            " LEFT JOIN t_s_resources D ON C.resource_id = D.id " +
            " WHERE 1=1 " +
            " and A.id = #{id} " +
            " and D.delete_flag = 0 and D.status = 1 and D.is_show = 1" +
            "</script>")
    List<Resources> selectResources(Integer id);

    @Select("<script>" +
            " SELECT distinct D.permission FROM t_s_user A " +
            " LEFT JOIN t_s_role_user B ON A.ID = B.user_id" +
            " LEFT JOIN t_s_role_resources C ON B.role_id = C.role_id " +
            " LEFT JOIN t_s_resources D ON C.resource_id = D.id " +
            " WHERE 1=1 " +
            " and A.id = #{id} " +
            " and D.delete_flag = 0 and D.status = 1 " +
            "</script>")
    List<String> selectPermissions(Integer id);
}
