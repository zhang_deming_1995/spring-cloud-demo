package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.Department;
import com.dm.cloud.api.dto.Log;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DepartmentDao extends BaseMapper<Department> {

    @Select("<script>" +
            " update t_s_department set status = #{status} ,update_date = now() ,update_by = #{username} " +
            " where 1=1" +
            " and id in " +
            " <foreach collection=\"list\" item=\"item\" index=\"index\" open=\"(\" close=\")\" separator=\",\"> " +
            "     #{item}" +
            " </foreach>"+
            "</script>")
    void invalidByIds(@Param("list") List<String> list,@Param("username") String username,@Param("status") Integer status);

}
