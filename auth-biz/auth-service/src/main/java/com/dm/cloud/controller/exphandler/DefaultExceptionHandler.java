package com.dm.cloud.controller.exphandler;

import com.dm.cloud.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class DefaultExceptionHandler {

    /**
     * 处理自定义异常
     */
    @ExceptionHandler({Exception.class})
    public R populationException(Exception e) {
        log.error(e.getMessage(), e);
        return R.failure("500",e.getMessage());
    }
}
