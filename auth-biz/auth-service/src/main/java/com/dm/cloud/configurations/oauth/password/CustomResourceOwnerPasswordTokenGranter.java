package com.dm.cloud.configurations.oauth.password;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.dm.cloud.api.dto.Users;
import com.dm.cloud.configurations.oauth.password.exceptions.*;
import com.dm.cloud.dao.UsersDao;
import com.dm.cloud.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Description:
 * 短信登陆鉴权 Provider，要求实现 AuthenticationProvider 模拟 ResourceOwnerPasswordTokenGranter类
 */
@Slf4j
public class CustomResourceOwnerPasswordTokenGranter extends AbstractTokenGranter {

    private UsersDao usersDao;

    private static final String GRANT_TYPE = "password";
    private final AuthenticationManager authenticationManager;

    public CustomResourceOwnerPasswordTokenGranter(AuthenticationManager authenticationManager, AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
        this(authenticationManager, tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
    }

    protected CustomResourceOwnerPasswordTokenGranter(AuthenticationManager authenticationManager, AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory, String grantType) {
        super(tokenServices, clientDetailsService, requestFactory, grantType);
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap(tokenRequest.getRequestParameters());
        String username = (String)parameters.get("username");
        String password = (String)parameters.get("password");
        parameters.remove("password");
        Authentication userAuth = new UsernamePasswordAuthenticationToken(username, password);
        ((AbstractAuthenticationToken)userAuth).setDetails(parameters);

        usersDao = SpringContextUtils.getBean(UsersDao.class);

        try {

            userAuth = this.authenticationManager.authenticate(userAuth);

            //账户密码正确 lock_flag 重置为 0
            UpdateWrapper<Users> udpa = new UpdateWrapper<>();
            udpa.setSql("lock_flag = 0 ");
            udpa.eq("user_name",username);
            Users updateDto = new Users();
            usersDao.update(updateDto, udpa);
        } catch (NonUsernameException var9) {
            throw new UsernameNotFoundException("用户不存在");
        } catch (AccountStatusException var8) {
            throw new InvalidGrantException(var8.getMessage());
        } catch (BadCredentialsException var10) {
            try {
                //账户密码错误 修改 lock_flag + 1
                UpdateWrapper<Users> udpa = new UpdateWrapper<>();
                udpa.setSql("lock_flag = lock_flag+1 ");
                udpa.eq("user_name",username);
                Users updateDto = new Users();
                usersDao.update(updateDto, udpa);
            }catch (Exception ex91){
                throw new RuntimeException("错误次数累加失败");
            }
            throw new InvalidGrantException("账号密码错误，错误三次将锁定账户");
        }

        if (userAuth != null && userAuth.isAuthenticated()) {
            OAuth2Request storedOAuth2Request = this.getRequestFactory().createOAuth2Request(client, tokenRequest);
            return new OAuth2Authentication(storedOAuth2Request, userAuth);
        } else {
            throw new InvalidGrantException("Could not authenticate user: " + username);
        }
    }
}