package com.dm.cloud.api.service;

import com.dm.cloud.api.vo.RolesVo;
import com.dm.cloud.common.R;

public interface IRolesService {

    R createRole(RolesVo rolesVo);

    R updateRole(RolesVo rolesVo);

    R deleteRole(RolesVo rolesVo);

    /**
     * 查询列表
     * @param users
     * @return
     */
    R selectPage(RolesVo roles);

    /**
     * 根据角色 ID 获取资源列表
     * @param roleId
     * @return
     */
    R getRoleById(String roleId);

}
