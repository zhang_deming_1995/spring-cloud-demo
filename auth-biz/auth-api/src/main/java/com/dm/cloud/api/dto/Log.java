package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("log")
public class Log {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 账户 */
    private String userName;
    /** 信息 */
    private String info;
    /** 等级 */
    private String level;
    /** 操作时间 */
    private Date operationTime;

    public void initLog(){
        this.operationTime = new Date();
    }
}
