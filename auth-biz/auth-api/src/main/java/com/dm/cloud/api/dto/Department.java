package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dm.cloud.common.CommonDto;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_s_department")
public class Department extends CommonDto {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 部门名称 */
    private String name;
    /** 上级部门ID */
    private String parentId;
    /** 排序 */
    private String sortNumber;
    /** 状态 0禁用 1启用 */
    private Integer status;
    /** 备注 */
    private String remark;
    /** 删除标志 0 未删除 1 已删除 */
    private Integer deleteFlag;

}
