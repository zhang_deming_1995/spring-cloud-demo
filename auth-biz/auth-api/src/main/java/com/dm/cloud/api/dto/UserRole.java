package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dm.cloud.common.CommonDto;
import lombok.Data;

@Data
@TableName("t_s_role_user")
public class UserRole {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 用户ID */
    private Integer userId;
    /** 角色ID */
    private Integer roleId;
}
