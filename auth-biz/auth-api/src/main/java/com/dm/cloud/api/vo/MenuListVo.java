package com.dm.cloud.api.vo;

import lombok.Data;

import java.util.List;

@Data
public class MenuListVo {
    //菜单
    private Integer id;
    //菜单
    private Integer sortNumber;
    //路径
    private String path;
    //名称
    private String name;
    //默认指向
    private String redirect;
    //LAYOUT
    private String component;
    //属性
    private MenuMeta meta;
    //孩子节点
    private List<MenuListVo> children;
    //类型
    private String type;
}
