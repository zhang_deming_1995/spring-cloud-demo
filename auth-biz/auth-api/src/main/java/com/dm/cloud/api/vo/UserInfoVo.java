package com.dm.cloud.api.vo;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
public class UserInfoVo {

    private Integer userId;

    private String username;

    private String realName;

    private String phoneNo;

    private String email;

    private String userDesc;

    private String avatar;

    private String homePath;

    public void setPhoneNo(String phoneNo) {
        //脱敏
        if (StringUtils.isEmpty(phoneNo) || (phoneNo.length() != 11)) {
            this.phoneNo = phoneNo;
        }
        this.phoneNo = phoneNo.replaceAll("(\\w{3})\\w*(\\w{4})", "$1****$2");
    }
}
