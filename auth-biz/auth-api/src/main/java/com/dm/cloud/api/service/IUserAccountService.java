package com.dm.cloud.api.service;

import com.dm.cloud.api.dto.Users;
import com.dm.cloud.api.vo.UserDetail;
import com.dm.cloud.api.vo.UserInfoVo;
import com.dm.cloud.api.vo.UserPublicMsg;
import com.dm.cloud.api.vo.UsersVo;
import com.dm.cloud.common.R;
import com.dm.cloud.utils.minio.UpDownloadEntity;

import java.util.List;

public interface IUserAccountService {

    /**
     * 获取当前登陆人用户ID
     * @param userInfo
     * @return
     */
    Integer getCurrentUserId();

    /**
     * 创建用户
     * @param users
     * @return
     */
    R createAccount(UsersVo usersVo, int type);

    /**
     * 更新用户信息 不允许修账户名 只允许更新副属性 名称、手机号、密码 等
     * @param users
     * @return
     */
    R updateAccount(UsersVo usersVo);

    /**
     * 删除账户
     * @return
     */
    R deleteAccount(UsersVo usersVo);

    /**
     * 请求验证码
     * @param phoneNo
     * @return
     */
    R smsCreate(String phoneNo, String type);

    /**
     * 登出账户
     * @return
     */
    R logoutAccount();

    /**
     * 查询当前用户下的菜单权限
     * @return
     */
    R getMenuList();

    /**
     * 查询指定用户的信息
     * @param id
     * @return
     */
    R searchAccount(String id);

    /**
     * 获取用户信息
     * @param uuid
     * @return
     */
    R<Users> getCurrentUser(String uuid);
    /**
     * 查询列表
     * @param users
     * @return
     */
    R selectPage(UsersVo usersVo);

    /**
     * 获取当前用户拥有的所有权限标识
     * @return
     */
    R getPermissions();

    /**
     * 获取当前用户的打败信息
     * @return
     */
    R getUserInfo();

    /**
     * 获取全部的用户列表
     * @return
     */
    R<List<UserDetail>> getUserList();

    /**
     * 更新个人信息
     * @param userInfo
     * @return
     */
    R updateUserInfo(UserInfoVo userInfo);

    /**
     * 获取头像上传下载地址
     * @param userId
     * @return
     */
    R<UpDownloadEntity> minioAvatarApi(Long userId);

    /**
     * 根据用户ID 获取开放信息
     * @param id
     * @return
     */
    R<UserPublicMsg> getUserPublicMsgById(Long id);
}
