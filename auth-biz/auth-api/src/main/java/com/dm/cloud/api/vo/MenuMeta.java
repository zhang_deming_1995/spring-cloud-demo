package com.dm.cloud.api.vo;

import lombok.Data;

@Data
public class MenuMeta {
    //标题名称
    private String title;
    //是否隐藏子层级
    private boolean hideChildrenInMenu;
    //图标志
    private String icon;
    //外链地址
    private String frameSrc;

    public MenuMeta(String title,boolean hideChildrenInMenu ,String icon){
        this.title = title;
        this.hideChildrenInMenu = hideChildrenInMenu;
        this.icon = icon;
    }

    public MenuMeta(String title,String icon){
        this.title = title;
        this.icon = icon;
    }
}
