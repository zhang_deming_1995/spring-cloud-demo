package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_s_department_user")
public class UserDepartment {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 用户ID */
    private Integer userId;
    /** 部门ID */
    private Integer departmentId;
}
