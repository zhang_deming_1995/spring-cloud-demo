package com.dm.cloud.api.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UsersVo {

    /** ID */
    private Integer id;
    /** 账户 */
    private String userName;
    /** 账户 */
    private String password;
    /** 账户 */
    private String smsCode;
    /** 部门ID */
    private Integer departmentId;
    /** 角色ID */
    private List<Integer> roleId;
    /** 用户名 */
    private String realName;
    /** 手机号 */
    private String phoneNo;
    /** 邮箱 */
    private String email;
    /** 注册时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
    private Date registerTime;
    /** 锁定标志 密码输入错误超过五次锁定 */
    private Integer lockFlag;
    /** 管理员禁用标志 */
    private Integer adminForbidden;
}
