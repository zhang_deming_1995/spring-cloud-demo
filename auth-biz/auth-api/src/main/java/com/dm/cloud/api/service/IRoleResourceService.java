package com.dm.cloud.api.service;

import com.dm.cloud.api.dto.RoleResource;
import com.dm.cloud.common.R;

public interface IRoleResourceService {

    R createRoleResources(RoleResource roleResource);

    R deleteRoleResources(RoleResource roleResource);
}
