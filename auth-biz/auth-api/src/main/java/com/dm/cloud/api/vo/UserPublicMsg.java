package com.dm.cloud.api.vo;

import lombok.Data;

@Data
public class UserPublicMsg {
    String realName; //用户名
    String avatar; //头像
}
