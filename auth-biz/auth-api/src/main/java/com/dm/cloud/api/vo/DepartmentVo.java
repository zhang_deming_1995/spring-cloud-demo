package com.dm.cloud.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class DepartmentVo {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 部门名称 */
    private String name;
    /** 排序 */
    private String sortNumber;
    /** 状态 0禁用 1启用 */
    private Integer status;
    /** 上级节点*/
    private String parentId;
    /** 备注 */
    private String remark;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
    private Date createDate;
    /** 子节点 */
    List<DepartmentVo> children;
    /** 类型 layout 根节点*/
    private String type;
}
