package com.dm.cloud.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dm.cloud.common.CommonDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class RolesVo extends CommonDto {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 角色编码 */
    private String roleCode;
    /** 角色名称 */
    private String roleName;
    /** 禁用标志 */
    private Integer status;
    /** 禁用标志 */
    private String remark;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
    private Date createDate;
    /** 资源列表 */
    private List<Integer> menu;
}
