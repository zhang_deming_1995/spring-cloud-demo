package com.dm.cloud.api.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("log")
public class LogVo {
    /** 信息 */
    private String info;
    /** 等级 */
    private String level;
    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
    private Date operationTime;
}
