package com.dm.cloud.api.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dm.cloud.common.CommonDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ResourcesVo extends CommonDto {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 标题 */
    private String title;
    /** 名称  */
    private String name;
    /** 父节点 */
    private String parentId;
    /** 排序 */
    private Integer sortNumber;
    /** 页面地址 */
    private String path;
    /** 组件路径 */
    private String url;
    /** 类型 0 目录 1 菜单 2 按钮 */
    private Integer type;
    /** 状态 0禁用1启用 */
    private Integer status;
    /** 是否外链 0否1是 */
    private Integer isOut;
    /** 是否显示 0否1是 */
    private Integer isShow;
    /** 是否缓存 0否1是 */
    private Integer isCache;
    /** 组件路径 */
    private String componentUrl;
    /** 图标 */
    private String icon ;
    /** 权限标识 */
    private String permission;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
    private Date createDate;
    /** 删除标志 未删除1已删除 */
    private Integer deleteFlag;
    /**
     * 子节点
     */
    List<ResourcesVo> children;

    /** 整理标志 用来处理整理菜单的时候 判断是否是用来接收结果的最外层结构 */
    private String sortType;
}
