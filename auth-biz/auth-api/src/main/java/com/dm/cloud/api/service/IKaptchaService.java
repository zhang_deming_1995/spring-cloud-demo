package com.dm.cloud.api.service;

import com.dm.cloud.common.R;

import javax.servlet.http.HttpServletRequest;

public interface IKaptchaService {

    /** 生成图形验证码 */
    R create(HttpServletRequest request);

    /** 校验图形验证码是否正确 */
    R checkImgCode(String data, HttpServletRequest request);
}
