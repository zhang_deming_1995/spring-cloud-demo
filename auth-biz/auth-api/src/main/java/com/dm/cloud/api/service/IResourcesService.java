package com.dm.cloud.api.service;

import com.dm.cloud.api.vo.ResourcesVo;
import com.dm.cloud.common.R;

public interface IResourcesService {

    R createResources(ResourcesVo resourcesVo);

    R updateResources(ResourcesVo resourcesVo);

    R deleteResources(ResourcesVo resourcesVo);

    R checkInvalid(ResourcesVo resourcesVo);

    R invalidResource(ResourcesVo resourcesVo);

    R selectPage(ResourcesVo resourcesVo);

    R searchResource(String id);

}
