package com.dm.cloud.api.vo;

import com.dm.cloud.api.dto.UserExtend;
import com.dm.cloud.api.dto.Users;
import lombok.Data;

@Data
public class UserDetail extends Users {
    UserExtend userExtend;
}
