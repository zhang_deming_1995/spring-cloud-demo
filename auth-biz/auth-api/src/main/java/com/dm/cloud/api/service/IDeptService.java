package com.dm.cloud.api.service;

import com.dm.cloud.api.vo.DepartmentVo;
import com.dm.cloud.common.R;

public interface IDeptService {

    R createDept(DepartmentVo deptVo);

    R updateDept(DepartmentVo deptVo);

    R checkInvalid(DepartmentVo deptVo);

    R invalidDept(DepartmentVo deptVo);

    R deleteDept(DepartmentVo deptVo);

    R selectPage(DepartmentVo deptVo);

    R searchDept(String id);

}
