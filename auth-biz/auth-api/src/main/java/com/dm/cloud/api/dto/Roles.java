package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dm.cloud.common.CommonDto;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_s_role")
public class Roles extends CommonDto {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 角色编码 */
    private String roleCode;
    /** 角色名称 */
    private String roleName;
    /** 禁用标志 */
    private Integer status;
    /** 备注 */
    private String remark;
    /** 删除标志 */
    private Integer deleteFlag;
}
