package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dm.cloud.common.CommonDto;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_s_resources")
public class Resources extends CommonDto {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 标题 */
    private String title;
    /** 父节点 */
    private String parentId;
    /** 排序 */
    private Integer sortNumber;
    /** 页面地址 */
    private String path;
    /** 路由 */
    private String url;
    /** 类型 0 目录 1 菜单 2 按钮 */
    private Integer type;
    /** 状态 0禁用1启用 */
    private Integer status;
    /** 是否外链 0否1是 */
    private Integer isOut;
    /** 是否显示 0否1是 */
    private Integer isShow;
    /** 是否缓存 0否1是 */
    private Integer isCache;
    /** 组件路径 */
    private String componentUrl;
    /** 图标 */
    private String icon ;
    /** 权限标识 */
    private String permission;
    /** 删除标志 未删除1已删除 */
    private Integer deleteFlag;

}
