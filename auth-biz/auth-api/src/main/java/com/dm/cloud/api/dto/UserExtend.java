package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_s_user_extend")
public class UserExtend {
    @TableId
    private Integer userId;
    /** 头像base64 */
    private String profilePhoto;
    /** 用户简介 */
    private String userDesc;
}
