package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@TableName("t_s_user")
public class Users {
    /** ID */
    @TableId(type= IdType.AUTO)
    private Integer id;
    /** 账户 */
    private String userName;
    /** 密码 */
    private String password;
    /** 用户名 */
    private String realName;
    /** 手机号 */
    private String phoneNo;
    /** 邮箱 */
    private String email;
    /** 注册时间 */
    private Date registerTime;
    /** 锁定标志 密码输入错误超过五次锁定 */
    private Integer lockFlag;
    /** 删除标志 */
    private Integer deleteFlag;
    /** 删除时间 */
    private Date deleteTime;
    /** 管理员禁用标志 */
    private Integer adminForbidden;
}
