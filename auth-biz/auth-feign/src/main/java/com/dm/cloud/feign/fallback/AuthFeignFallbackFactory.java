package com.dm.cloud.feign.fallback;

import com.dm.cloud.feign.auth.AuthFeign;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class AuthFeignFallbackFactory implements FallbackFactory<AuthFeign> {
    @Override
    public AuthFeign create(Throwable throwable) {
        AuthFeignFallback accountFeignFallback = new AuthFeignFallback();
        accountFeignFallback.setCause(throwable);
        return accountFeignFallback;
    }
}