package com.dm.cloud.feign.fallback;

import com.dm.cloud.api.dto.Users;
import com.dm.cloud.api.vo.UserDetail;
import com.dm.cloud.common.R;
import com.dm.cloud.feign.auth.AuthFeign;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class AuthFeignFallback implements AuthFeign {
    @Setter
    private Throwable cause;

    @Override
    public R<Users> getCurrentUser(String uuid){
        log.error("查询失败,接口异常" ,cause);
        return R.failure("500","接口熔断");
    }

    @Override
    public R<List<UserDetail>> getUserList(){
        log.error("查询失败,接口异常" ,cause);
        return R.failure("500","接口熔断");
    }
}