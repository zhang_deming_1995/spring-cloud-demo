package com.dm.cloud.feign.auth;

import com.dm.cloud.api.dto.Users;
import com.dm.cloud.api.vo.UserDetail;
import com.dm.cloud.common.R;
import com.dm.cloud.feign.fallback.AuthFeignFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "auth-service",fallbackFactory = AuthFeignFallbackFactory.class)
public interface AuthFeign {

    @GetMapping(value = "/user/getCurrentUser/{uuid}")
    R<Users> getCurrentUser(@PathVariable("uuid") String uuid);

    @GetMapping(value = "/user/getUserList")
    R<List<UserDetail>> getUserList();

}