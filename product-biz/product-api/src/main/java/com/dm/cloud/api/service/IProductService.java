package com.dm.cloud.api.service;

import com.dm.cloud.api.dto.Product;
import com.dm.cloud.common.R;

public interface IProductService {

    /* 增加 */
    R<Long> insert(Product account);

    /* 删除 */
    R<Long> deleteById(Product account);

    /* 修改 */
    R<Long> updateById(Product account);

    /* 按照账号查询 */
    R<Product> selectByCode(String accountCode);

    /* 按照账号查询 */
    R<Boolean> reduce(String productCode, int count);


}
