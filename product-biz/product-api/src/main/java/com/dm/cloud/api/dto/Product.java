package com.dm.cloud.api.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product {
    private int Id;
    private String productCode;
    private String productName;
    private int count;
    private BigDecimal price;
}
