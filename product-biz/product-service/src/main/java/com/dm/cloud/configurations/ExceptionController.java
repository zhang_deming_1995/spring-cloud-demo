package com.dm.cloud.configurations;

import com.dm.cloud.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
public class ExceptionController {
 
    @ResponseBody
    @ExceptionHandler(value = Exception.class)    //异常处理器
    public R<Object> hanlerException(HttpServletRequest request, Exception e){
        log.error("请求{}出错:{}",request.getRequestURI(),e.getMessage());
        return R.failure("500",e.getMessage());
    }
}