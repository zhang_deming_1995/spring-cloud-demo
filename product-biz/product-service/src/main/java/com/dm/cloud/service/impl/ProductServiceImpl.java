package com.dm.cloud.service.impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dm.cloud.api.dto.Product;
import com.dm.cloud.api.service.IProductService;
import com.dm.cloud.common.R;
import com.dm.cloud.dao.ProductDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public R<Long> insert(Product product) {
        try{
            return R.success(Long.valueOf(productDao.insert(product)));
        }catch (Exception ex){
            log.error(String.format("商品信息插入失败:[%s]", JSONUtils.toJSONString(product)),ex);
            return R.failure("1104",String.format("商品信息插入失败:[%s]", JSONUtils.toJSONString(product)));
        }
    }

    @Override
    public R<Long> deleteById(Product product) {
        try{
            return R.success(Long.valueOf(productDao.deleteById(product.getId())));
        }catch (Exception ex){
            log.error(String.format("商品信息删除失败:[%s]", JSONUtils.toJSONString(product)),ex);
            return R.failure("1105",String.format("商品信息删除失败:[%s]", JSONUtils.toJSONString(product)));
        }
    }

    @Override
    public R<Long> updateById(Product product) {
        try{
            return R.success(Long.valueOf(productDao.updateById(product)));
        }catch (Exception ex){
            log.error(String.format("商品信息更新失败:[%s]", JSONUtils.toJSONString(product)),ex);
            return R.failure("1106",String.format("商品信息更新失败:[%s]", JSONUtils.toJSONString(product)));
        }
    }

    @Override
    public R<Product> selectByCode(String productCode) {
        QueryWrapper<Product> productQueryWrapper =new QueryWrapper<>();
        productQueryWrapper.eq("product_code",productCode);
        productQueryWrapper.last(" limit 1");
        try{
            return R.success(productDao.selectOne(productQueryWrapper));
        }catch (Exception ex){
            log.error(String.format("商品信息查询失败:编码->[%s]", productCode),ex);
            return R.failure("1107",String.format("商品信息查询失败:编码->[%s]", productCode));
        }
    }

    /**
     * 扣减库存
     * @param productCode
     * @param count
     * @return
     */
    @Override
    @Transactional
    public R<Boolean> reduce(String productCode, int count) {


        //拆线呢商品信息
        QueryWrapper<Product> productQueryWrapper =new QueryWrapper<>();
        productQueryWrapper.eq("product_code",productCode);
        productQueryWrapper.last(" limit 1");
        Product product = productDao.selectOne(productQueryWrapper);
        //判断商品是否存在
        if(null == product){
            log.error(String.format("商品【%s】信息不存在", productCode));
            return R.failure("1101","商品信息不存在");
        }
        //计算库存是否满足扣减数量
        int surplus = product.getCount() - count;
        if(surplus < 0){
            log.error(String.format("商品【%s】库存不足", productCode));
            return R.failure("1102",String.format("商品【%s】库存不足，当前剩余【%s】",product.getProductName(),product.getCount()));
        }
        product.setCount(surplus);
        //修改库存信息
        if(productDao.updateById(product)==0){
            log.error(String.format("商品【%s】扣减数量【%d】失败", productCode,count));
            return R.failure("1103",String.format("商品【%d】扣减数量【%s】失败", productCode,count));
        }

        return R.success(true);
    }
}
