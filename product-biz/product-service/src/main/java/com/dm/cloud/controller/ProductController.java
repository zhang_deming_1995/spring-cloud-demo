package com.dm.cloud.controller;

import com.dm.cloud.api.dto.Product;
import com.dm.cloud.api.service.IProductService;
import com.dm.cloud.common.R;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping("product")
public class ProductController {

    @Autowired
    private IProductService orderService;

    @GetMapping("/search/{productCode}")
    public R<Product> search(@PathVariable("productCode") String productCode){
        log.info("get order detail,orderNo is :{}",productCode);
        return orderService.selectByCode(productCode);
    }

    @PostMapping("/insert")
    public R<Long> insert(@RequestBody Product product){
        log.info("insert info :{}",product);
        return orderService.insert(product);
    }

    @PostMapping("/update")
    public R<Long> update(@RequestBody Product product){
        log.info("update info :{}",product);
        return orderService.updateById(product);
    }

    @PostMapping("/delete")
    public R<Long> delete(@RequestBody Product product){
        log.info("delete info :{}",product);
        return orderService.deleteById(product);
    }

    @GetMapping("/reduce/{productCode}/{count}")
    public R<Boolean> reduce(@PathVariable("productCode") String productCode, @PathVariable("count") int count){
        log.info("reduce info :{}{}",productCode,count);
        return orderService.reduce(productCode,count);
    }
}
