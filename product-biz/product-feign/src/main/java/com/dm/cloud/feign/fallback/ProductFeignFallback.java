package com.dm.cloud.feign.fallback;

import com.dm.cloud.api.dto.Product;
import com.dm.cloud.common.R;
import com.dm.cloud.feign.product.ProductFeign;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProductFeignFallback implements ProductFeign {
    @Setter
    private Throwable cause;

    @Override
    public R<Product> search(String productCode){
        log.error("查询失败,接口异常" ,cause);
        return R.failure("500","接口熔断");
    }

    @Override
    public R<Long> insert(Product product){
        return R.failure("500","接口熔断");
    }

    @Override
    public R<Long> update(Product product){
        return R.failure("500","接口熔断");
    }

    @Override
    public R<Long> delete(Product product){
        return R.failure("500","接口熔断");
    }

    @Override
    public R<Boolean> reduce(String productCode, int count){
        return R.failure("500","接口熔断");
    }

}