package com.dm.cloud.feign.product;

import com.dm.cloud.api.dto.Product;
import com.dm.cloud.common.R;
import com.dm.cloud.feign.fallback.ProductFeignFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "product-service",fallbackFactory = ProductFeignFallbackFactory.class)
public interface ProductFeign {

    @GetMapping("/product/search/{accountCode}")
    R<Product> search(@PathVariable("accountCode") String productCode);

    @PostMapping("/product/insert")
    R<Long> insert(@RequestBody Product product);

    @PostMapping("/product/update")
    R<Long> update(@RequestBody Product product);

    @PostMapping("/product/delete")
    R<Long> delete(@RequestBody Product product);

    @GetMapping("/product/reduce/{productCode}/{count}")
//    @RequestMapping(method = RequestMethod.GET, value = "/product/reduce/{productCode}/{count}", consumes = "application/json")
    R<Boolean> reduce(@PathVariable("productCode") String productCode, @PathVariable("count") int count);
}