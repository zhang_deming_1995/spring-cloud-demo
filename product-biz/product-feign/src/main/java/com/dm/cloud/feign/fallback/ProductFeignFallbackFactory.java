package com.dm.cloud.feign.fallback;

import com.dm.cloud.feign.product.ProductFeign;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class ProductFeignFallbackFactory implements FallbackFactory<ProductFeign> {
    @Override
    public ProductFeign create(Throwable throwable) {
        ProductFeignFallback productFeignFallback = new ProductFeignFallback();
        productFeignFallback.setCause(throwable);
        return productFeignFallback;
    }
}