package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.Orders;
import com.dm.cloud.api.dto.Seckill;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SeckillDao extends BaseMapper<Seckill> {

}
