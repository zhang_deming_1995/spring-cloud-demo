package com.dm.cloud.service.impl;

import com.alibaba.nacos.common.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dm.cloud.api.dto.Orders;
import com.dm.cloud.api.dto.Product;
import com.dm.cloud.api.service.IOrderService;
import com.dm.cloud.common.R;
import com.dm.cloud.dao.OrderDao;
import com.dm.cloud.feign.account.AccountFeign;
import com.dm.cloud.feign.product.ProductFeign;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    OrderDao orderDao;

    @Autowired
    private AccountFeign accountFeign;

    @Autowired
    private ProductFeign productFeign;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R<Long> insert(Orders account) {
        try{
            return R.success(Long.valueOf(orderDao.insert(account)));
        }catch (Exception ex){
            return R.failure("新增出错！");
        }
    }

    @Override
    public R<Long> deleteById(Orders account) {
        try{
            return R.success(Long.valueOf(orderDao.deleteById(account.getId())));
        }catch (Exception ex){
            return R.failure("删除出错！");
        }
    }

    @Override
    public R<Long> updateById(Orders account) {
        try{
            return R.success(Long.valueOf(orderDao.updateById(account)));
        }catch (Exception ex){
            return R.failure("更新出错！");
        }
    }

    @Override
    public R<Orders> selectByCode(String accountCode) {
        QueryWrapper<Orders> accountQueryWrapper =new QueryWrapper<>();
        accountQueryWrapper.eq("order_no",accountCode);
        accountQueryWrapper.last(" limit 1");
        try{
            return R.success(orderDao.selectOne(accountQueryWrapper));
        }catch (Exception ex){
            return R.failure("查询出错！");
        }
    }

    @Override
//    @GlobalTransactional(name = "TX_ORDER_CREATE",rollbackFor = Exception.class)//开启全局事务
    @Transactional  //要一起用才能生效？
    public R<Orders> orderCreate(Orders orders){
        orders.setOrderNo(UUID.randomUUID().toString());
        //根据商品编码获取商品价格
        String productCode= orders.getProductId().toString();
        if(StringUtils.isNotBlank(productCode)){
            //查询单价
            R<Product> productRsl = productFeign.search(productCode);

            if(!"success".equals(productRsl.getType())){
                return R.failure(productRsl.getCode(),productRsl.getMessage());
            }

            if(null != Optional.ofNullable(productRsl.getResult()).orElse(new Product()).getPrice()){

                orders.setPaymentAmount(productRsl.getResult().getPrice().multiply(new BigDecimal(orders.getCount())));

                //记录订单信息
                Orders ordersNew = new Orders();
                BeanUtils.copyProperties(orders, ordersNew);
                //本地存储Order
                ordersNew.setId(0l);
                orderDao.insert(ordersNew);
                //库存扣减
                R productReduceRsl= productFeign.reduce(orders.getProductId().toString(), orders.getCount());
                if(!"success".equals(productReduceRsl.getType())){
                    //抛出异常
                    throw new RuntimeException(productReduceRsl.getMessage());
                }
                //账户余额扣减
                R accountReduceRsl= accountFeign.reduce(orders.getAccountId().toString(), orders.getPaymentAmount());;
                if(!"success".equals(accountReduceRsl.getType())){
                    //抛出异常
                    throw new RuntimeException(accountReduceRsl.getMessage());
                }
                return R.success(ordersNew);
            }else{
                return R.failure("0","商品信息查询失败！");
            }
        }else{
            return R.failure("0","缺少商品信息！");
        }
    }
}
