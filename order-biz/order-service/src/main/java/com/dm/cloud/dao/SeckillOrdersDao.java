package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.SeckillOrders;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SeckillOrdersDao extends BaseMapper<SeckillOrders> {

}
