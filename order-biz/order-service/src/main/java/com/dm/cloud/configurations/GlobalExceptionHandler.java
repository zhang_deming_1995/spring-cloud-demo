package com.dm.cloud.configurations;

import com.dm.cloud.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: 全局异常处理 (aop)
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R handleException(Exception e, HttpServletRequest request) {
        log.warn("请求url:[{}]出错", request.getRequestURL(), e);
        return R.failure("500",e.getMessage());
    }

}