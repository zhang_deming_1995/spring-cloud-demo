package com.dm.cloud.api.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Orders {
    /** ID */
    private Long Id;
    /** 订单号 */
    private String orderNo;
    /** 账户ID */
    private Long accountId;
    /** 商品ID */
    private Long productId;
    /** 数量 */
    private Integer count;
    /** 支付金额 */
    private BigDecimal paymentAmount;
    /** 订单类型 */
    private Integer type;
}
