package com.dm.cloud.api.service;

import com.dm.cloud.common.R;

public interface ISeckillService {

    /* 秒杀订单*/
    R seckill(Long accountId,Long pid) throws InterruptedException;

    R killpay(String orderNo) throws InterruptedException;

    void payTest();
}
