package com.dm.cloud.api.service;

import com.dm.cloud.api.dto.Orders;
import com.dm.cloud.common.R;

public interface IOrderService {

    /* 增加 */
    R<Long> insert(Orders account);

    /* 删除 */
    R<Long> deleteById(Orders account);

    /* 修改 */
    R<Long> updateById(Orders account);

    /* 按照账号查询 */
    R<Orders> selectByCode(String accountCode);

    /* 创建订单 */
    R<Orders> orderCreate(Orders orders);
}
