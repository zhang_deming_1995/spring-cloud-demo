package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Seckill {
    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;
    /** 商品ID */
    private Long productId;
    /** 秒杀数量 */
    private Integer count;
    /** 秒杀价格 */
    private BigDecimal seckillPrice;
    /** 秒杀开始时间 */
    private Date startTime;
    /** 秒杀结束时间 */
    private Date endTime;
    /** 是否存入缓存 */
    private Boolean isCached;
}
