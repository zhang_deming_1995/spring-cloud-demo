package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class SeckillOrders implements Serializable {
    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;
    /** 订单号 */
    private String orderNo;
    /** 账户ID */
    private Long accountId;
    /** 秒杀ID */
    private Long seckillId;
    /** 秒杀数量 */
    private Integer count;
    /** 应付金额 */
    private BigDecimal paymentAmount;
    /** 下单时间 */
    private Date checkoutTime;
    /** 状态 0待支付 1已支付 2已取消 */
    private Integer status;
    /** 处理类型 0 创建订单 1 扣减库存 2 恢复库存 3 修改订单状态 */
    //应该是一个单独的类放这个值 这里偷懒了
    @TableField(exist = false)
    private Integer rabbitMqType;
}
