package com.dm.cloud.configurations;

import com.dm.cloud.common.CloudConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.ArrayList;
import java.util.Map;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;

@Component
@Order(1)
public class GatewayRequestFilter implements GlobalFilter,Ordered {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        byte[] token = Base64Utils.encode((CloudConstant.GATEWAY_TOKEN_VALUE).getBytes());
        String[] headerValues = {new String(token)};

        exchange.getRequest()
                .mutate()
                .header(CloudConstant.GATEWAY_TOKEN_HEADER, headerValues);

        ServerHttpRequest build ;

        Map<String, String> stringStringMap = exchange.getRequest().getHeaders().toSingleValueMap();
//        获取子协议
        String Protocol_auth = exchange.getRequest().getHeaders().getFirst("Sec-WebSocket-Protocol");
        if(stringStringMap.containsKey("authorization")){
            String authorization =stringStringMap.get("authorization");
            UserDetails user =  Oauth2Utils.getByAuthorization(authorization);
            build = exchange.getRequest()
                    .mutate()
                    .header(CloudConstant.GATEWAY_TOKEN_HEADER, headerValues)
                    //补充用户信息
                    .header(CloudConstant.GATEWAY_USERNAME,null == user ?"":user.getUsername())
                    .build();
        }else if(StringUtils.isNotEmpty(Protocol_auth)){
            MultiValueMap<String, String> params = exchange.getRequest().getQueryParams();
            build = exchange.getRequest()
                    .mutate()
                    .header(CloudConstant.GATEWAY_TOKEN_HEADER, headerValues)
                    .header("Authorization", "Bearer "+Protocol_auth)
                    .build();
        }else{
            build = exchange.getRequest()
                    .mutate()
                    .header(CloudConstant.GATEWAY_TOKEN_HEADER, headerValues)
                    .build();
        }

        ServerWebExchange newExchange = exchange.mutate().request(build).build();

        return chain.filter(newExchange);
    }


    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE - 2;
    }

}