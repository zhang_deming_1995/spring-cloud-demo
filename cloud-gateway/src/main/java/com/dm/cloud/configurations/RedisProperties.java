package com.dm.cloud.configurations;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "custom.datasource.redis")
public class RedisProperties {
    private String ip;
    private int port;
    private int smsExpire;
}
