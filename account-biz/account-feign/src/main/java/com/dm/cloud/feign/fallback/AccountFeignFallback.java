package com.dm.cloud.feign.fallback;

import com.dm.cloud.api.dto.Account;
import com.dm.cloud.common.R;
import com.dm.cloud.feign.account.AccountFeign;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
public class AccountFeignFallback implements AccountFeign {
    @Setter
    private Throwable cause;

    @Override
    public R<Account> search(String accountCode){
        log.error("查询失败,接口异常" ,cause);
        return R.failure("500","接口熔断");
    }

    @Override
    public R<Long> insert(Account account){
        return R.failure("500","接口熔断");
    }

    @Override
    public R<Long> update(Account account){
        return R.failure("500","接口熔断");
    }

    @Override
    public R<Long> delete(Account account){
        return R.failure("500","接口熔断");
    }

    @Override
    public R<Boolean> reduce(String accountCode, BigDecimal amount){
        return R.failure("500","接口熔断");
    }

}