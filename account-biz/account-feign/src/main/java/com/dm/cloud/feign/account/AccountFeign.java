package com.dm.cloud.feign.account;

import com.dm.cloud.api.dto.Account;
import com.dm.cloud.common.R;
import com.dm.cloud.feign.fallback.AccountFeignFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;

@FeignClient(name = "account-service",fallbackFactory = AccountFeignFallbackFactory.class)
public interface AccountFeign {

    @GetMapping("/account/search/{accountCode}")
    R<Account> search(@PathVariable("accountCode") String accountCode);

    @PostMapping("/account/insert")
    R<Long> insert(@RequestBody Account account);

    @PostMapping("/account/update")
    R<Long> update(@RequestBody Account account);

    @PostMapping("/account/delete")
    R<Long> delete(@RequestBody Account account);

    @GetMapping("/account/reduce/{accountCode}/{amount}")
    R<Boolean> reduce(@PathVariable("accountCode") String accountCode, @PathVariable("amount") BigDecimal amount);
}