package com.dm.cloud.feign.fallback;

import com.dm.cloud.feign.account.AccountFeign;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class AccountFeignFallbackFactory implements FallbackFactory<AccountFeign> {
    @Override
    public AccountFeign create(Throwable throwable) {
        AccountFeignFallback accountFeignFallback = new AccountFeignFallback();
        accountFeignFallback.setCause(throwable);
        return accountFeignFallback;
    }
}