package com.dm.cloud.service.impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.dm.cloud.api.dto.Account;
import com.dm.cloud.api.service.IAccountService;
import com.dm.cloud.common.R;
import com.dm.cloud.dao.AccountDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@Slf4j
public class AccountServiceImpl implements IAccountService {

    @Autowired
    AccountDao accountDao;

    @Override
    public R<Long> insert(Account account, String db) {
        try{
            return R.success(Long.valueOf(accountDao.insert(account)));
        }catch (Exception ex){
            log.error(String.format("账号信息插入失败:[%s]", JSONUtils.toJSONString(account)),ex);
            return R.failure("1004",String.format("账号信息插入失败:[%s]", JSONUtils.toJSONString(account)));
        }
    }

    @Override
    public R<Long> deleteById(Account account, String db) {
        try{
            return R.success(Long.valueOf(accountDao.deleteById(account.getId())));
        }catch (Exception ex){
            log.error(String.format("账号信息删除出错:[%s]", JSONUtils.toJSONString(account)),ex);
            return R.failure("1005",String.format("账号信息删除失败:[%s]", JSONUtils.toJSONString(account)));
        }
    }

    @Override
    public R<Long> updateById(Account account, String db) {
        try{
            return R.success(Long.valueOf(accountDao.updateById(account)));
        }catch (Exception ex){
            log.error(String.format("账号信息更新出错:[%s]", JSONUtils.toJSONString(account)),ex);
            return R.failure("1006",String.format("账号信息更新失败:[%s]", JSONUtils.toJSONString(account)));
        }
    }

    @Override
    public R<Account> selectByCode(String accountCode, String db) {
        QueryWrapper<Account> accountQueryWrapper =new QueryWrapper<>();
        accountQueryWrapper.eq("account_code",accountCode);
        accountQueryWrapper.last(" limit 1");
        try{
            Account resl = accountDao.selectOne(accountQueryWrapper);
            if(null != resl){
                return R.success(resl);
            }else{
                return R.failure("1008",String.format("账号信息不存在:账号->[%s]", accountCode));
            }
        }catch (Exception ex){
            log.error(String.format("账号信息查询出错:账号->[%s]", accountCode),ex);
            return R.failure("1007",String.format("账号信息查询出错:账号->[%s]", accountCode));
        }
    }

    @Override
    @Transactional
    public R<Boolean> reduce(String accountCode, BigDecimal amount, String db) {
        //查询账户信息
        QueryWrapper<Account> accountQueryWrapper =new QueryWrapper<>();
        accountQueryWrapper.eq("account_code",accountCode);
        accountQueryWrapper.last(" limit 1");
        Account account = accountDao.selectOne(accountQueryWrapper);
        //判断账户信息是否存在
        if(null == account){
            log.error(String.format("账户【%s】信息不存在", accountCode));
            return R.failure("1001",String.format("账户信息不存在", accountCode));
        }
        //判断账户余额是否满足
        BigDecimal subAmount = account.getAmount().subtract(amount);
        if(subAmount.compareTo(BigDecimal.ZERO) < 0){
            log.error(String.format("账户【%s】余额不足", accountCode));
            return R.failure("1002",String.format("账户余额不足"));
        }
        account.setAmount(subAmount);
        if(accountDao.updateById(account)==0){
            log.error(String.format("账号【%s】扣减余额【%s】失败", account,String.valueOf(amount)));
            return R.failure("1003",String.format("账号余额扣减失败"));
        }
        return R.success(true);
    }
}
