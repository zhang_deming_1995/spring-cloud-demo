package com.dm.cloud.api.service;

import com.dm.cloud.common.R;
import com.dm.cloud.api.dto.Account;

import java.math.BigDecimal;

public interface IAccountService {
    /* 增加 */
    R<Long> insert(Account account, String db);

    /* 删除 */
    R<Long> deleteById(Account account, String db);

    /* 修改 */
    R<Long> updateById(Account account, String db);

    /* 按照账号查询 */
    R<Account> selectByCode(String accountCode, String db);

    /* 扣减账户余额 */
    R<Boolean> reduce(String accountCode, BigDecimal amount, String db);


}
