package com.dm.cloud.api.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Account {
    private int Id;
    private String accountCode;//账号
    private String accountName;//账户名称
    private BigDecimal amount;//账户余额
}
