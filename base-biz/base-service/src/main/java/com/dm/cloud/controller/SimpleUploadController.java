package com.dm.cloud.controller;

import com.dm.cloud.api.service.IFUserFileService;
import com.dm.cloud.common.R;
import com.dm.cloud.controller.base.BaseExcelController;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 简单上传
 */
@Slf4j
@RestController
@RequestMapping("/simple_upload")
@AllArgsConstructor
public class SimpleUploadController extends BaseExcelController {
    @Autowired
    IFUserFileService service;

    /**
     * 获取上传文件地址（同名文件覆盖）
     */
    @GetMapping("/minio/default/api")
    public R minioSimpleApi(String objectName) {
        return service.minioSimpleApi(objectName);
    }
}
