package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.FUserFileTask;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 图表数据接口
 * </p>
 *
 * @author zhangdeming
 */
@Mapper
public interface FUserFileTaskDao extends BaseMapper<FUserFileTask> {

}
