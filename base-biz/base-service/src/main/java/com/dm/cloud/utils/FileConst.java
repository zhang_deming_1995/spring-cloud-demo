package com.dm.cloud.utils;

public class FileConst {
    /**
     * 文件类型标志
     */
    public static final int FILE_TYPE_PICTURE = 1;
    public static final int FILE_TYPE_VEDIO = 2;
    public static final int FILE_TYPE_DOCUMENT = 3;
    public static final int FILE_TYPE_MUSIC = 4;
    public static final int FILE_TYPE_SEED = 5;
    public static final int FILE_TYPE_OTHERS = 6;
    /**
     * 文件公开状态
     */
    public static final int FILE_SHARE_TYPE_PRIVATE = 0 ;
    public static final int FILE_SHARE_TYPE_PUBLIC = 1 ;
    /**
     * 文件传输状态
     */
    public static final int UPLOAD_FILE_STATUS_WAIT = 0 ;
    public static final int UPLOAD_FILE_STATUS_TRANSFER = 1 ;
    public static final int UPLOAD_FILE_STATUS_PAUSE = 2 ;
    public static final int UPLOAD_FILE_STATUS_FINISHED = 3 ;
    public static final int UPLOAD_FILE_STATUS_STOPED = 9 ;
    /**
     * 传输进度标志
     */
    public static final String UPLOAD_FILE_PROCESS_PREFIX = "upload_file_process_prefix:" ;
    /**
     * 用于上传的锁
     */
    public static final String UPLOAD_FILE_MD5_LOCK_PREFIX = "upload_file_md5_lock_prefix:" ;
    public static final String UPLOAD_FILE_CREATE_TASK_LOCK_PREFIX = "upload_file_create_task_lock_prefix:" ;
}
