package com.dm.cloud.controller;

import com.dm.cloud.api.service.IOaCityCoordinateService;
import com.dm.cloud.api.vo.OaCityCoordinateVo;
import com.dm.cloud.common.R;
import com.dm.cloud.controller.base.BaseExcelController;
import com.dm.cloud.utils.easyexcel.DefaultImportListener;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/data/oa_city_coor")
@AllArgsConstructor
public class DataOaCityCoordinateController extends BaseExcelController {

    @Autowired
    private final IOaCityCoordinateService service;

    //页表查询
    @GetMapping("search/{id}")
    public R search(@PathVariable("id") String id) {
        return service.search(id);
    }

    //页表查询
    @PostMapping("page")
    public R page(@RequestBody OaCityCoordinateVo condition) {
        return service.page(condition);
    }

    //新增
    @PostMapping("create")
    public R create(@RequestBody OaCityCoordinateVo condition) {
        return service.create(condition);
    }

    //修改
    @PostMapping("update")
    public R update(@RequestBody OaCityCoordinateVo condition) {
        return service.update(condition);
    }

    //批量 删除
    @PostMapping("delete")
    public R delete(@RequestBody OaCityCoordinateVo condition) {
        return service.delete(condition);
    }

    //导入数据
    @PostMapping("import")
    public R importData(@RequestBody MultipartFile file) {
        DefaultImportListener<OaCityCoordinateVo> importListener = new DefaultImportListener<OaCityCoordinateVo>(service);
        importExcel(file, OaCityCoordinateVo.class, importListener, "城市坐标导入");
        //在线导入
        return R.data("文件导入成功！");
    }

    //导出模板
    @GetMapping(value = "/templet")
    public void exportTemplet(HttpServletResponse response) throws IOException {
        List<OaCityCoordinateVo> list = new ArrayList<>();
        exportTemplate(response, "城市坐标", OaCityCoordinateVo.class, list, false);
    }

    //导出数据
    @GetMapping(value = "/export")
    public void exportData(OaCityCoordinateVo condition,HttpServletResponse response) throws IOException {
        List<OaCityCoordinateVo> list = service.list(condition);
        exportTemplate(response, "城市坐标", OaCityCoordinateVo.class, list, false);
    }

}
