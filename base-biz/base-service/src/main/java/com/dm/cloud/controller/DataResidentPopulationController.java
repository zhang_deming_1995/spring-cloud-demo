package com.dm.cloud.controller;

import com.dm.cloud.api.service.IResidentPopulationService;
import com.dm.cloud.api.vo.ResidentPopulationVo;
import com.dm.cloud.common.R;
import com.dm.cloud.controller.base.BaseExcelController;
import com.dm.cloud.utils.easyexcel.DefaultImportListener;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/data/resident_population")
@AllArgsConstructor
public class DataResidentPopulationController extends BaseExcelController {

    @Autowired
    private final IResidentPopulationService service;


    //单条信息查询
    @GetMapping("search/{id}")
    public R search(@PathVariable("id") String id) {
        return service.search(id);
    }

    //页表查询
    @PostMapping("page")
    public R page(@RequestBody ResidentPopulationVo condition) {
        return service.page2(condition);
    }

    //新增
    @PostMapping("create")
    public R create(@RequestBody ResidentPopulationVo condition) {
        return service.create(condition);
    }

    //修改
    @PostMapping("update")
    public R update(@RequestBody ResidentPopulationVo condition) {
        return service.update2(condition);
    }

    //批量 删除
    @PostMapping("delete")
    public R delete(@RequestBody ResidentPopulationVo condition) {
        return service.delete(condition);
    }

    //导入数据
    @PostMapping("import")
    public R importData(@RequestBody MultipartFile file) {
        DefaultImportListener<ResidentPopulationVo> importListener = new DefaultImportListener<ResidentPopulationVo>(service);
        importExcel(file, ResidentPopulationVo.class, importListener, "城市坐标导入");
        //在线导入
        return R.data("文件导入成功！");
    }

    //导出模板
    @GetMapping(value = "/templet")
    public void exportTemplet(HttpServletResponse response) throws IOException {
        List<ResidentPopulationVo> list = new ArrayList<>();
        exportTemplate(response, "城市常住规模模板.xlsx", ResidentPopulationVo.class, list, false);
    }

    //导出数据
    @GetMapping(value = "/export")
    public void exportData(ResidentPopulationVo condition,HttpServletResponse response) throws IOException {
        List<ResidentPopulationVo> list = service.list2(condition);
        exportTemplate(response, "城市常住规模列表.xlsx", ResidentPopulationVo.class, list, false);
    }
}
