package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.ChatCache;
import com.dm.cloud.api.vo.ChatMessageVo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 图表数据接口
 * </p>
 *
 * @author zhangdeming
 */
@Mapper
public interface ChatCacheDao extends BaseMapper<ChatCache> {

    @Insert("<script>" +
            " insert into chat_cache(sender_id,receiver_id,content_type,message_content) values" +
            " <foreach collection=\"list\" index=\"index\" item=\"item\" separator=\",\">\n" +
            " ( #{item.senderId}, #{item.receiverId}, #{item.contentType}, #{item.message} )" +
            " </foreach>" +
            "</script>")
    Integer batchInsert(@Param("list") List<ChatMessageVo> list);

}
