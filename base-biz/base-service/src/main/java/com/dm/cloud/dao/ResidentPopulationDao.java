package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.ResidentPopulation;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ResidentPopulationDao extends BaseMapper<ResidentPopulation> {

    @Select("SELECT DISTINCT `year` FROM `resident_population` WHERE type=#{type} ORDER BY `year` DESC LIMIT 30")
    List<Integer> getYearList(@Param("type") Integer type);

    @Select("<script>" +
            " update resident_population " +
            " <trim prefix=\"set\" suffixOverrides=\",\"> " +
            "    <trim prefix=\"longitude = case\" suffix=\"end,\"> " +
            "        <foreach collection=\"list\" item=\"item\"> " +
            "            <if test=\"item.longitude!=null\"> " +
            "                when id=#{item.id} then #{item.longitude} " +
            "            </if> " +
            "        </foreach> " +
            "    </trim> " +
            "    <trim prefix=\"latitude = case\" suffix=\"end,\"> " +
            "        <foreach collection=\"list\" item=\"item\"> " +
            "            <if test=\"item.latitude!=null\"> " +
            "                when id=#{item.id} then #{item.latitude} " +
            "            </if> " +
            "        </foreach> " +
            "    </trim> " +
            " </trim> " +
            " <where> " +
            " id in " +
            "    <foreach collection=\"list\" index=\"index\" item=\"item\" open=\"(\" close =\")\" separator=\",\"> " +
            "        #{item.id} " +
            "    </foreach> " +
            " </where>" +
            "</script>")
    void bacthUpdateCoor(@Param("list") List<ResidentPopulation> list);

    /**
     * 批量插入
     *
     */
    @Insert("<script>" +
            "INSERT INTO resident_population " +
            "(type,year,city_name,population_number,longitude,latitude) " +
            "VALUES " +
            "<foreach collection=\"list\" index=\"index\" item=\"item\" separator=\",\">\n" +
            "( #{item.type},#{item.year},#{item.cityName},#{item.populationNumber},#{item.longitude},#{item.latitude} )" +
            "</foreach>" +
            "</script>")
    Integer batchInsert(@Param("list") List<ResidentPopulation> list);
}
