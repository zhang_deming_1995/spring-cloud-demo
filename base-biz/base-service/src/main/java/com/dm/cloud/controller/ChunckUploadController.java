package com.dm.cloud.controller;

import com.alibaba.fastjson.JSONObject;
import com.dm.cloud.api.dto.FUserFileTask;
import com.dm.cloud.api.service.IFUserFileService;
import com.dm.cloud.api.vo.FUserFileTaskVo;
import com.dm.cloud.api.vo.FUserFileVo;
import com.dm.cloud.api.vo.FUserFolderVo;
import com.dm.cloud.api.vo.ResidentPopulationVo;
import com.dm.cloud.common.R;
import com.dm.cloud.controller.base.BaseExcelController;
import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 支持 断点续传
 * 采用MD5算法判断文件内容是否相同，可能出现碰撞问题 即 不同文件的MD5值是相同的
 * 但是可能性极低 >> 链接已失效
 */
@Slf4j
@RestController
@RequestMapping("/chunck_upload")
@AllArgsConstructor
public class ChunckUploadController extends BaseExcelController {

    @Autowired
    IFUserFileService service;

    /**
     * 获取文件夹层级信息
     */
    @PostMapping("/folder/tree")
    public R foldTree(@RequestBody FUserFolderVo userFolderVo) {
        return service.foldTree(userFolderVo);
    }

    /**
     * 创建文件夹
     */
    @PostMapping("/folder/create")
    public R foldCreate(@RequestBody FUserFolderVo userFolderVo) {
        return service.foldCreate(userFolderVo);
    }

    /**
     * 修改文件夹信息
     */
    @PostMapping("/folder/update")
    public R foldUpdate(@RequestBody FUserFolderVo userFolderVo) {
        return service.foldUpdate(userFolderVo);
    }

    /**
     * 删除文件夹
     */
    @PostMapping("/folder/delete")
    public R foldDelete(@RequestBody FUserFolderVo userFolderVo) {
        return service.foldDelete(userFolderVo);
    }

    /**
     * 获取文件列表 分层级查询
     */
    @PostMapping("list")
    public R fileList(@RequestBody FUserFileVo condition){
        return service.getFileList(condition);
    }

    /**
     * 获取公开文件列表
     */
    @PostMapping("page/public")
    public R filePublicList(@RequestBody FUserFileVo condition){
        return service.filePublicList(condition);
    }

    /**
     * 修改文件名
     */
    @PostMapping("update")
    public R updateFile(@RequestBody FUserFileVo condition){
        return service.updateFile(condition);
    }

    /**
     * 删除文件
     * 只删除用户关联关系
     * 实际文件要看是否有其他用户关联 若没有则删除
     */
    @PostMapping("delete")
    public R deleteFile(@RequestBody FUserFileVo condition){
        return service.deleteFile(condition);
    }

    /**
     * 创建传输任务
     */
    @PostMapping("createTask")
    public R createTask(@RequestBody FUserFileTaskVo condition){
        return service.createTask(condition);
    }

    /**
     * 分片初始化 获取上传地址
     * @param requestParam 请求参数-此处简单处理
     * @return /
     */
    @PostMapping("/init")
    public R initMultiPartInit(@RequestBody JSONObject requestParam) {
        // md5-可进行秒传判断
        String objectName = requestParam.getString("objectName");
        Assert.notNull(objectName,"分片文件MD5缺失");
        // 分片数量
        Integer partCount = Optional.of(requestParam.getIntValue("partCount")).orElse(1);
        // 分片数量
        String contentType = requestParam.getString("contentType");

        return service.initMultiPartUpload(partCount,objectName,contentType);
    }

    /**
     * 完成上传 触发合并分片
     * @param requestParam 用户参数
     * @return /
     */
    @PostMapping("/merge")
    public R completeMultiPartUpload(@RequestBody JSONObject requestParam) {
        Integer tid = requestParam.getInteger("tid");
        String md5 = requestParam.getString("md5");
        String uploadId = requestParam.getString("uploadId");
        Integer chuncks = requestParam.getInteger("chuncks");
        boolean finished = requestParam.getBoolean("finished");
        Assert.notNull(md5, "md5 must not be null");
        Assert.notNull(finished, "finished must not be null");
        return service.mergeMultipartUpload(tid,md5, uploadId,chuncks, finished);
    }

    /**
     * 删除分片上传任务
     * @return /
     */
    @PostMapping("/removeMultiTask")
    public R removeMultiTask(String uploadId,String objectName) {
        return service.removeMultiTask(uploadId,objectName);
    }

    /**
     * 删除分片上传任务
     * @return /
     */
    @PostMapping("/clearMultiTask")
    public R clearMultiTask(String objectName) {
        return service.clearMultiTask(objectName);
    }

    /**
     * 设置生命周期
     * @return /
     */
    @PostMapping("/addLifeCycle")
    public R addLifeCycle() {
        return service.addLifeCycle();
    }

    /**
     * 删除生命周期
     * @return /
     */
    @PostMapping("/deleteLifeCycle")
    public R deleteLifeCycle() {
        return service.deleteLifeCycle();
    }

    /**
     * 获取下载路径
     * @return /
     */
    @GetMapping("/getUploadUrl/{id}")
    public R getDownLoadUrl(@PathVariable("id") Integer id) {
        return service.getDownLoadUrl(id);
    }

    /**
     * office文件预览
     * @return /
     */
    @GetMapping("/openOfficePreview/{id}")
    public R openOfficePreview(@PathVariable("id") Integer id) {
        return service.openOfficePreview(id);
    }
}
