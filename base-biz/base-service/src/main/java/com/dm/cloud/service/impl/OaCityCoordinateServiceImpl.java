package com.dm.cloud.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dm.cloud.api.dto.OaCityCoordinate;
import com.dm.cloud.api.service.IOaCityCoordinateService;
import com.dm.cloud.api.vo.OaCityCoordinateVo;
import com.dm.cloud.common.R;
import com.dm.cloud.dao.OaCityCoordinateDao;
import com.dm.cloud.utils.sql.SqlUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**

 */
@Slf4j
@Service
public class OaCityCoordinateServiceImpl extends ServiceImpl<OaCityCoordinateDao, OaCityCoordinate> implements IOaCityCoordinateService {

    @Override
    public R search(String id) {
        return R.data(baseMapper.selectById(id));
    }

    @Override
    public R create(OaCityCoordinateVo condition) {

        SqlUtils.valid(condition);

        OaCityCoordinate ins = new OaCityCoordinate();
        BeanUtils.copyProperties(condition,ins);

        //城市信息保持唯一
        QueryWrapper wapper = new QueryWrapper();
        wapper.eq("city",ins.getCity());

        int count = baseMapper.selectCount(wapper);
        if(count>0){
            //修改已存在的行
            baseMapper.update(ins,wapper);
        }else{
            //插入行
            baseMapper.insert(ins);
        }
        return R.data("保存成功");
    }

    @Override
    public R update(OaCityCoordinateVo condition) {

        SqlUtils.valid(condition);

        OaCityCoordinate ins = new OaCityCoordinate();
        BeanUtils.copyProperties(condition,ins);

        QueryWrapper wapper = new QueryWrapper();
        wapper.eq("city",ins.getCity());
        baseMapper.update(ins,wapper);

        return R.data("保存成功");
    }

    @Override
    public R delete(OaCityCoordinateVo condition) {
        baseMapper.deleteById(condition.getId());
        return R.data("删除成功");
    }

    @Override
    @Transactional
    public void importExcel(List<OaCityCoordinateVo> list) {
        if(list==null || list.size()==0){
            throw new RuntimeException("导入列表为空！");
        }
        list.stream().forEach(e->SqlUtils.valid(e));

        //清除当前表 重新导入
        LambdaUpdateWrapper<OaCityCoordinate> wrapper = Wrappers.<OaCityCoordinate>lambdaUpdate();

        baseMapper.delete(wrapper);
        //再批量插入 分批插入
        int size =5000;
        if(list.size()>size){
            for(int i=0;i<list.size();i+=size){
                doInsert(list,i,size);
            }
        }else{
            doInsert(list,0,list.size());
        }
    }

    /**
     * 整理集合、插入数据
     * @param list
     * @param startPos
     * @param batchSize
     */
    private void doInsert(List<OaCityCoordinateVo> list,int startPos,int batchSize){
        List<OaCityCoordinate> insertList = new ArrayList<>();
        //整理插入信息
        for (int j=startPos;j<Math.min(startPos+batchSize,list.size());j++) {
            OaCityCoordinate insert = new OaCityCoordinate();
            insert.setCity(list.get(j).getCity());
            insert.setLongitude(list.get(j).getLongitude());
            insert.setLatitude(list.get(j).getLatitude());
            insertList.add(insert);
        }
        //批量插入
        baseMapper.batchInsert(insertList);
    }

    /**
     * 分页查询
     * @return
     */
    @Override
    public R page(OaCityCoordinateVo condition) {
        IPage<OaCityCoordinate> page = new Page<>();
        page.setSize(condition.getPageSize());
        page.setCurrent(condition.getPage());

        QueryWrapper<OaCityCoordinate> wrapper = new QueryWrapper();
        if(StringUtils.isNotBlank(condition.getCity())){
            wrapper.like("city",condition.getCity());
        }
        if(ObjectUtils.isNotNull(condition.getLongitude())){
            wrapper.like("longitude",condition.getLongitude());
        }
        if(ObjectUtils.isNotNull(condition.getLatitude())){
            wrapper.like("latitude",condition.getLatitude());
        }

        //列表排序
        SqlUtils.order(wrapper,condition);
        return R.data(this.page(page, wrapper));
    }

    /**
     * 获取导出列表
     * @param condition
     * @return
     */
    @Override
    public List<OaCityCoordinateVo> list(OaCityCoordinateVo condition) {
        //初始化查询条件
        QueryWrapper<OaCityCoordinate> wrapper = new QueryWrapper();
        if(StringUtils.isNotBlank(condition.getCity())){
            wrapper.like("city",condition.getCity());
        }
        if(ObjectUtils.isNotNull(condition.getLongitude())){
            wrapper.like("longitude",condition.getLongitude());
        }
        if(ObjectUtils.isNotNull(condition.getLatitude())){
            wrapper.like("latitude",condition.getLatitude());
        }
        //列表排序
        SqlUtils.order(wrapper,condition);
        //转换为EXCEL到处需要的实体类
        return this.list(wrapper).stream()
            .map(e -> {
                OaCityCoordinateVo tExcel = new OaCityCoordinateVo();
                BeanUtils.copyProperties(e,tExcel);
                return tExcel;
            }).collect(Collectors.toList());
    }
}