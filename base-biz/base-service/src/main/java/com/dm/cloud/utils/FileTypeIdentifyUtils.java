package com.dm.cloud.utils;

import java.util.HashMap;
import java.util.Map;

public class FileTypeIdentifyUtils {

    /**
     * 暂时卸载程序中 文件类型在初始化时就已经确认 不允许修改
     * 后续可以调整为在前端根据后缀识别
     */
    private static Map<String,Integer> fileTypes = new HashMap<>();

    {
        //待补充
        if(fileTypes.isEmpty()){
            //添加图片类型
            addPic("BMP").addPic("DIB").addPic("PCP").addPic("DIF").addPic("WMF").addPic("GIF").addPic("JPG").addPic("MPT")
            .addPic("TIF").addPic("EPS").addPic("PSD").addPic("CDR").addPic("IFF").addPic("TGA").addPic("PCD").addPic("PNG");
            //添加视频类型
            addVedio("AVI").addVedio("WMV").addVedio("MPG").addVedio("MPEG").addVedio("MOV").addVedio("RM").addVedio("RAM").addVedio("SWF").addVedio("FLV").addVedio("MP4");
            //添加音乐类型
            addMusic("WAV").addMusic("MIDI").addMusic("CDA").addMusic("MP3").addMusic("WMA").addMusic("APE").addMusic("FLAC");
            //添加文档类型
            addDoc("DOC").addDoc("DOCX").addDoc("XLS").addDoc("XLSX").addDoc("PPT").addDoc("PPTX").addDoc("WPS").addDoc("DPS").addDoc("ET");
            //添加种子类型
            addSeed("TORRENT");
        }
    }

    public Map<String,Integer> getAllTypes(){
        return new HashMap<>(fileTypes);
    }

    /**
     * 根据文件后缀 返回文件类型标志
     * @param fileSuffix
     * @return
     */
    public static Integer identify(String fileSuffix){
        String key = fileSuffix.toUpperCase();
        if(fileTypes.containsKey(key)){
            return fileTypes.get(key);
        }else{
            return FileConst.FILE_TYPE_OTHERS;
        }
    }

    //添加照片
    private FileTypeIdentifyUtils addPic(String suffix){
        fileTypes.put(suffix, FileConst.FILE_TYPE_PICTURE);
        return this;
    }
    //添加视频
    private FileTypeIdentifyUtils addVedio(String suffix){
        fileTypes.put(suffix, FileConst.FILE_TYPE_VEDIO);
        return this;
    }
    //添加文档
    private FileTypeIdentifyUtils addDoc(String suffix){
        fileTypes.put(suffix, FileConst.FILE_TYPE_DOCUMENT);
        return this;
    }
    //添加音乐
    private FileTypeIdentifyUtils addMusic(String suffix){
        fileTypes.put(suffix, FileConst.FILE_TYPE_MUSIC);
        return this;
    }
    //添加种子
    private FileTypeIdentifyUtils addSeed(String suffix){
        fileTypes.put(suffix, FileConst.FILE_TYPE_SEED);
        return this;
    }
}
