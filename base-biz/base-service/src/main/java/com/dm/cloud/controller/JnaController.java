package com.dm.cloud.controller;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 工作流相关接口
 */
@Slf4j
@RestController
@RequestMapping("/jna")
@AllArgsConstructor
public class JnaController {

    public interface JnaLibrary extends Library {
        JnaLibrary INSTANCE = Native.load("winTest2", JnaLibrary.class); // 引入add.so动态库

        JnaLibrary.Point Jna_add(JnaLibrary.Point.ByValue ponit); // 定义使用方式
        String TransString1(); // 定义使用方式
        String TransString(String left, String right); // 定义使用方式

        @Structure.FieldOrder({"x", "y", "z"}) // 构建结构体
        public static class Point extends Structure {
            public Point() {}
            public static class UserValue extends JnaLibrary.Point implements Structure.ByValue {
                public UserValue(float x, float y, float z) {
                    super(x, y, z);
                }
            }

            public Point(float x, float y, float z) {
                this.x = x;
                this.y = y;
                this.z = z;
            }

            public float x;
            public float y;
            public float z;
        }
    }


    /**
     *
     * @return
     */
    @GetMapping("test1")
    public void test1() {
        JnaLibrary.Point.UserValue startPoint = new JnaLibrary.Point.UserValue(1, 2, 3);
        JnaLibrary.Point a = JnaLibrary.INSTANCE.Jna_add(startPoint);

        System.out.println(a.x);
        System.out.println(a.y);
        System.out.println(a.z);

        String result1 = JnaLibrary.INSTANCE.TransString1();
        System.out.println(result1);


        String result2 = JnaLibrary.INSTANCE.TransString("aaa","bbb");
        System.out.println(result2);
    }
}
