package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.FUserFile;
import com.dm.cloud.api.dto.FUserFolder;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 图表数据接口
 * </p>
 *
 * @author zhangdeming
 */
@Mapper
public interface FUserFolderDao extends BaseMapper<FUserFolder> {

}
