package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.OaCityCoordinate;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 图表数据接口
 * </p>
 *
 * @author zhangdeming
 */
@Mapper
public interface OaCityCoordinateDao extends BaseMapper<OaCityCoordinate> {

    /**
     * 批量插入
     *
     */
    @Insert("<script>" +
            "INSERT INTO oa_city_coordinate " +
            "( city, longitude, latitude) " +
            "VALUES " +
            "<foreach collection=\"list\" index=\"index\" item=\"item\" separator=\",\">\n" +
            "( #{item.city}, #{item.longitude}, #{item.latitude} )" +
            "</foreach>" +
            "</script>")
    Integer batchInsert(@Param("list") List<OaCityCoordinate> list);
}
