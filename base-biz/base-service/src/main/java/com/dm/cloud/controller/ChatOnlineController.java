package com.dm.cloud.controller;

import com.dm.cloud.api.service.IChatOnlineService;
import com.dm.cloud.common.R;
import com.dm.cloud.controller.base.BaseExcelController;
//import com.dm.cloud.dao.CassandraCacheDao;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/chat_online")
@AllArgsConstructor
public class ChatOnlineController {

    @Autowired
    IChatOnlineService service;

    /**
     * 查询所有的用户id信息
     * @param id
     * @param datetime
     * @return
     */
    @GetMapping("user_list")
    public R getUsersList() {
        return R.data(service.getUsersList());
    }

    /**
     * 查询指定接收人的聊天记录
     * 传一个时间节点 获取改时间节点之后的聊天内容
     * @param id
     * @param datetime
     * @return
     */
    @GetMapping("search_cache/{id}")
    public R searchCache(@PathVariable("id") String id,String datetime) {
        return R.data(service.searchCache(id,datetime));
    }
}
