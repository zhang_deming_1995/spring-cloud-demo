package com.dm.cloud.controller;

import com.dm.cloud.api.service.IActivitiService;
import com.dm.cloud.common.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 工作流相关接口
 */
@Slf4j
@RestController
@RequestMapping("/activiti")
@AllArgsConstructor
public class ActivitiController {

    @Autowired
    IActivitiService service;

    /**
     *
     * @return
     */
    @PostMapping("add")
    public R add() {
        return R.data(service.add());
    }

    /**
     *
     * @return
     */
    @PostMapping("update")
    public R update() {
        return R.data(service.update());
    }

    /**
     *
     * @return
     */
    @PostMapping("delete")
    public R delete() {
        return R.data(service.delete());
    }

    /**
     *
     * @return
     */
    @PostMapping("disable")
    public R disable() {
        return R.data(service.disable());
    }

    /**
     *
     * @return
     */
    @PostMapping("enable")
    public R enable() {
        return R.data(service.enable());
    }
}
