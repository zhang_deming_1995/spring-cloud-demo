package com.dm.cloud.service.impl;

import com.dm.cloud.api.service.IActivitiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**

 */
@Slf4j
@Service
public class ActivitiServiceImpl implements IActivitiService {


    @Override
    public int add() {
        return 0;
    }

    @Override
    public int update() {
        return 0;
    }

    @Override
    public int delete() {
        return 0;
    }

    @Override
    public int disable() {
        return 0;
    }

    @Override
    public int enable() {
        return 0;
    }
}