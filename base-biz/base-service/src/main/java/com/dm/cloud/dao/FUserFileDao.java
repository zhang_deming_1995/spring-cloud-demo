package com.dm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dm.cloud.api.dto.FUserFile;
import com.dm.cloud.api.dto.RoleResource;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 图表数据接口
 * </p>
 *
 * @author zhangdeming
 */
@Mapper
public interface FUserFileDao extends BaseMapper<FUserFile> {

    @Insert("<script>" +
            "INSERT INTO f_user_file " +
            "(user_id,folder_id,md5,file_name,public_flag,upload_time ) " +
            "VALUES " +
            "<foreach collection=\"list\" index=\"index\" item=\"item\" separator=\",\">\n" +
            " ( #{item.userId},#{item.folderId},#{item.md5},#{item.fileName},#{item.publicFlag},#{item.uploadTime} )" +
            "</foreach>" +
            "</script>")
    long batchInsert(@Param("list")List<FUserFile> insertFiles);
}
