package com.dm.cloud.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Data
public class ChatMessageVo implements Serializable,Cloneable {
	private static final long serialVersionUID = 1L;

	/**
	 * 消息类型
	 */
	private Integer messageType;
	/**
	 * uuid 消息ID
	 */
	private String uuid;
	/**
	 * 消息内容
	 */
	private String message;
	/**
	 * 在线用户列表
	 */
	private Set<String> userLists;

	/**
	 * 在线人数
	 */
	private Integer number;

	/**
	 * 消息发送人
	 */
	private String senderId;

	/**
	 * 消息接收人
	 */
	private String receiverId;

	/**
	 * 内容类型
	 */
	private Integer contentType;

	/**
	 * 内容类型
	 */
	private Date sendTime;

	/**
	 * 头像
	 */
	private String avatar;

	@Override
	public ChatMessageVo clone() {
		try {
			return (ChatMessageVo)super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e.getCause());
		}
	}


}
