package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.dm.cloud.utils.pager.PageVo;
import lombok.Data;

import java.io.Serializable;
import java.time.DateTimeException;
import java.util.Date;

@Data
public class FUserFileTask implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;

	/**
	 * 用户ID
	 */
	private Integer userId;

	/**
	 * 文件夹ID
	 */
	private Integer folderId;

	/**
	 * MD5
	 */
	private String md5;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 传输类型 1上传 2下载
	 */
	private Integer type;
	/**
	 * 状态 0暂停 1 传输中 3传输完成
	 */
	private Integer status;
	/**
	 * 状态 0 普通 1 急速
	 */
	private Integer transferType;
	/**
	 * 创建时间
	 */
	private Date createTime;
}
