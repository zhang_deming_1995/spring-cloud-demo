package com.dm.cloud.api.vo;

import com.dm.cloud.utils.pager.PageVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FUserFileTaskVo extends PageVo implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	private Integer id;

	/**
	 * 用户ID
	 */
	private Integer userId;

	/**
	 * 文件夹ID
	 */
	private Integer folderId;

	/**
	 * MD5
	 */
	private String md5;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 用户本地的文件地址
	 */
	private String fileAddress;
	/**
	 * 传输类型 1上传 2下载
	 */
	private Integer type;
	/**
	 * 状态 0等待传输 1 传输中 2暂停 3传输完成
	 */
	private Integer status;
	/**
	 * 文件类型
	 * 1、图片
	 * 2、视频
	 * 3、文档
	 * 4、音乐
	 * 5、种子
	 * 6、其他
	 */
//	private Integer fileType;
	/**
	 * 进度条状态
	 */
	private Double progress;
	/**
	 * 创建时间
	 */
	private Date createTime;
}
