package com.dm.cloud.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dm.cloud.api.dto.ResidentPopulation;
import com.dm.cloud.api.vo.ResidentPopulationVo;
import com.dm.cloud.common.R;
import com.dm.cloud.utils.easyexcel.DefaultImportService;

import java.util.List;

public interface IResidentPopulationService extends IService<ResidentPopulation>, DefaultImportService<ResidentPopulationVo> {
    /**
     * 单条信息查询
     * @param id
     * @return
     */
    R search(String id);

    /**
     * 分页查询
     * @param condition
     * @return
     */
    R page2(ResidentPopulationVo condition);

    /**
     * 添加
     * @param condition
     * @return
     */
    R create(ResidentPopulationVo condition);

    /**
     * 修改
     * @param condition
     * @return
     */
    R update2(ResidentPopulationVo condition);

    /**
     * 删除
     * @param condition
     * @return
     */
    R delete(ResidentPopulationVo condition);

    /**
     * 查询列表
     * @param condition
     * @return
     */
    List<ResidentPopulationVo> list2(ResidentPopulationVo condition);
}

