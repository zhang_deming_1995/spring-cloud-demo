package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("f_user_file")
public class FUserFile implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * userId
	 */
	@TableField("user_id")
	private Integer userId;

	/**
	 * 文件夹ID
	 */
	@TableField("folder_id")
	private Integer folderId;

	/**
	 * md5
	 */
	@TableField("md5")
	private String md5;

	/**
	 * 文件名称
	 */
	@TableField("file_name")
	private String fileName;

	/**
	 * 上传时间
	 */
	@TableField("upload_time")
	private Date uploadTime;

	/**
	 * 是否公开 0否 1是
	 */
	@TableField("public_flag")
	private Integer publicFlag;
}
