package com.dm.cloud.api.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.dm.cloud.annotions.PopRequired;
import com.dm.cloud.utils.pager.PageVo;
import lombok.Data;

import java.io.Serializable;

/**
 * 测试代码
 */
@Data
public class OaCityCoordinateVo extends PageVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ExcelIgnore
    @ExcelProperty(value = "id")
    private String id;

    @PopRequired(msg= "城市不能为空")
    @ExcelProperty(value = "城市")
    private String city;

    @PopRequired(msg= "经度不能为空")
    @ExcelProperty(value = "经度")
    private Double longitude;

    @PopRequired(msg= "纬度不能为空")
    @ExcelProperty(value = "纬度")
    private Double latitude;
}