package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("chat_cache")
public class ChatCache implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * ID标志
	 */
	private Integer id;
	/**
	 * 发送人ID
	 */
	private Integer senderId;
	/**
	 * 接收人ID（为群预留 群为group_开头）
	 */
	private String receiverId;
	/**
	 * 内容类型
	 * 1、普通文本
	 * 2、图片
	 * 3、视频
	 */
	private Integer contentType;
	/**
	 * 消息内容
	 */
	private String messageContent;
	/**
	 * 消息发送时间
	 */
	private Date sendTime;
}
