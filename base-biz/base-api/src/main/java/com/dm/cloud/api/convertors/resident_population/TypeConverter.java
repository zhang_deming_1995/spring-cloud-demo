package com.dm.cloud.api.convertors.resident_population;

import com.alibaba.excel.converters.Converter;
import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.GlobalConfiguration;
import com.alibaba.excel.metadata.property.ExcelContentProperty;

/**
 * 方案类型转换
 *
 * @author huyanchao
 */
public class TypeConverter implements Converter<Integer> {


    @Override
    public Class<?> supportJavaTypeKey() {
        return String.class;
    }

    @Override
    public CellDataTypeEnum supportExcelTypeKey() {
        return null;
    }

    @Override
    public Integer convertToJavaData(CellData cellData, ExcelContentProperty excelContentProperty, GlobalConfiguration globalConfiguration) {
        String type = cellData.getStringValue();
        if ("省内城市".equals(type)) {
            return 1;
        } else if ("副省级城市".equals(type)) {
            return 2;
        }else if ("人口过千万城市".equals(type)) {
            return 3;
        }else if ("超大城市".equals(type)) {
            return 4;
        }else if ("特大城市".equals(type)) {
            return 5;
        }else if ("山东省".equals(type)) {
            return 6;
        }else if ("全国".equals(type)) {
            return 7;
        }else if ("主要对比城市".equals(type)) {
            return 8;
        }else{
            return 0;
        }
    }

    @Override
    public CellData<Integer> convertToExcelData(Integer type, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        if (1 == type) {
            return new CellData<>("省内城市");
        } else if (2 == type){
            return new CellData<>("副省级城市");
        }else if (3 == type){
            return new CellData<>("人口过千万城市");
        }else if (4 == type){
            return new CellData<>("超大城市");
        }else if (5 == type){
            return new CellData<>("特大城市");
        }else if (6 == type){
            return new CellData<>("山东省");
        }else if (7 == type){
            return new CellData<>("全国");
        }else if (8 == type){
            return new CellData<>("主要对比城市");
        }else {
            return new CellData<>("未知类型");
        }
    }
}
