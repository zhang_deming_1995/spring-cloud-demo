package com.dm.cloud.api.service;

import com.dm.cloud.api.vo.OaCityCoordinateVo;
import com.dm.cloud.common.R;
import com.dm.cloud.utils.easyexcel.DefaultImportService;

import java.util.List;

/**
 * 测试接口
 */
public interface IOaCityCoordinateService extends DefaultImportService<OaCityCoordinateVo> {

    /**
     * 分页查询
     * @param year
     * @param page
     * @return
     */
    R page(OaCityCoordinateVo condition);

    /**
     * 查询列表
     * @param condition
     * @return
     */
    List<OaCityCoordinateVo> list(OaCityCoordinateVo condition);

    /**
     * 新增
     * @param condition
     * @return
     */
    R create(OaCityCoordinateVo condition);

    /**
     * 修改
     * @param condition
     * @return
     */
    R update(OaCityCoordinateVo condition);


    /**
     * 删除
     * @param condition
     * @return
     */
    R delete(OaCityCoordinateVo condition);

    /**
     * 查询单条
     * @param id
     * @return
     */
    R search(String id);
}

