package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FUserFolder implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;

	/**
	 * 用户ID
	 */
	private Integer userId;

	/**
	 * 父文件夹
	 */
	private Integer parentFolderId;

	/**
	 * 文件夹
	 */
	private String folder;
	/**
	 * 创建时间
	 */
	private Date createTime;
}
