package com.dm.cloud.api.service;

import com.dm.cloud.api.vo.*;
import com.dm.cloud.common.R;
import com.dm.cloud.utils.easyexcel.DefaultImportService;
import com.dm.cloud.utils.minio.UpDownloadEntity;

import java.util.List;
import java.util.Map;

/**
 * 测试接口
 */
public interface IFUserFileService {


    R foldTree(FUserFolderVo userFolderVo);

    R foldCreate(FUserFolderVo userFolderVo);

    R foldUpdate(FUserFolderVo userFolderVo);

    R foldDelete(FUserFolderVo userFolderVo);

    /**
     * 查询列表
     * @return
     */
    R getFileList(FUserFileVo condition);
    /**
     * 查询公开文件列表
     * @return
     */
    R filePublicList(FUserFileVo condition);
    /**
     * 删除文件
     * @param condition
     * @return
     */
    R deleteFile(FUserFileVo condition);

    /**
     * 分片上传初始化
     *
     * @param partCount   分片数量
     * @param contentType /
     * @return /
     */
    R initMultiPartUpload(Integer partCount,String md5,String contentType);

    /**
     * 完成分片上传
     *
     * @param uploadId 标识
     * @return /
     */
    R mergeMultipartUpload(Integer tid,String md5, String uploadId,Integer chuncks, boolean finished);

    /**
     * 创建传输任务
     * @param condition
     * @return
     */
    R createTask(FUserFileTaskVo condition);

    /**
     * 修改文件名
     * @param condition
     * @return
     */
    R updateFile(FUserFileVo condition);

    /**
     * 获取下载路径
     * @param id
     * @return
     */
    R getDownLoadUrl(Integer id);

    /**
     * office文件预览接口
     * @param id
     * @return
     */
    R openOfficePreview(Integer id);

    /**
     * 取消上传任务
     * @param uploadId
     * @return
     */
    R removeMultiTask(String uploadId,String objectName);

    /**
     * 清理上传任务
     * @param objectName
     * @return
     */
    R clearMultiTask(String objectName);

    /**
     * 设置生命周期
     * @return
     */
    R addLifeCycle();

    /**
     * 删除生命周期
     * @return
     */
    R deleteLifeCycle();

    //简单上传接口
    /**
     * Minio 单文件上传
     * @return
     */
    R<UpDownloadEntity> minioSimpleApi(String objectName);
}

