package com.dm.cloud.api.vo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.dm.cloud.annotions.PopRequired;
import com.dm.cloud.api.convertors.resident_population.TypeConverter;
import com.dm.cloud.utils.pager.PageVo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 测试代码
 */
@Data
public class ResidentPopulationVo extends PageVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ExcelIgnore
    @ExcelProperty(value = "id")
    private Integer id;
    /**
     * 数据类型：1-省内城市  2-计划单列市  3-人口过千万城市  4-超大城市  5-特大城市
     */
    @ExcelProperty(value = "类型",converter = TypeConverter.class)
    @PopRequired(msg = "类型不能为空")
    private Integer type;
    /**
     * 年份
     */
    @ExcelProperty(value = "年份")
    @PopRequired(msg = "年份不能为空")
    private Integer year;
    /**
     * 城市名称
     */
    @ExcelProperty(value = "城市")
    @PopRequired(msg = "城市名称不能为空")
    private String cityName;
    /**
     * 常住人口数量（万人）
     */
    @ExcelProperty(value = "常住人口数量")
    @PopRequired(msg = "人口规模不能为空")
    private BigDecimal populationNumber;
    /**
     * 经度
     */
    @ExcelProperty(value = "经度")
    private String longitude;
    /**
     * 纬度
     */
    @ExcelProperty(value = "纬度")
    private String latitude;
    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;
}