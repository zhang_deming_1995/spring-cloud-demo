package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("resident_population")
public class ResidentPopulation implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 数据类型：1-省内城市  2-计划单列市  3-人口过千万城市  4-超大城市  5-特大城市
	 */
	@TableField("type")
	private Integer type;
	/**
	 * 年份
	 */
	@TableField("year")
	private Integer year;
	/**
	 * 城市名称
	 */
	@TableField("city_name")
	private String cityName;
	/**
	 * 常住人口数量（万人）
	 */
	@TableField("population_number")
	private BigDecimal populationNumber;
	/**
	 * 经度
	 */
	@TableField("longitude")
	private String longitude;
	/**
	 * 纬度
	 */
	@TableField("latitude")
	private String latitude;
	/**
	 *
	 */
	@TableField("create_time")
	private Date createTime;

}
