package com.dm.cloud.api.vo;

import com.dm.cloud.utils.pager.PageVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FUserFolderVo implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	private Integer id;

	/**
	 * 用户ID
	 */
	private Integer userId;

	/**
	 * 文件夹ID
	 */
	private Integer parentFolderId;

	/**
	 * md5
	 */
	private String folder;
}
