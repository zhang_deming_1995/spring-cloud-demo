package com.dm.cloud.api.service;

import com.dm.cloud.api.vo.ChatMessageVo;

import java.util.List;

/**
 * 在线聊天接口
 */
public interface IChatOnlineService {

    List<Object> getUsersList();

    List<ChatMessageVo> searchCache(String id, String datetime);
}

