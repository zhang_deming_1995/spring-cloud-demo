package com.dm.cloud.api.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * </p>
 *
 * @author zhangdeming
 * @since 2021-11-17
 */
@Data
@TableName(value = "oa_city_coordinate")
public class OaCityCoordinate implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String city;

    private Double longitude;

    private Double latitude;
}
