package com.dm.cloud.api.service;

/**
 * 工作流接口
 */
public interface IActivitiService {

    /**
     * 创建工作流
     * @return
     */
    int add();

    /**
     * 修改工作流信息
     * @return
     */
    int update();

    /**
     * 删除工作流信息
     * @return
     */
    int delete();

    /**
     * 禁用工作流
     * @return
     */
    int disable();

    /**
     * 启用工作流
     * @return
     */
    int enable();

}

