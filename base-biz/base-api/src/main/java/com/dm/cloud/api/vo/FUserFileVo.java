package com.dm.cloud.api.vo;

import com.dm.cloud.utils.pager.PageVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FUserFileVo extends PageVo implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	private Integer id;

	/**
	 * 用户ID
	 */
	private String userId;

	/**
	 * 文件夹ID
	 */
	private String folderId;

	/**
	 * md5
	 */
	private String md5;

	/**
	 * 文件名称
	 */
	private String fileName;

	/**
	 * 上传时间
	 */
	private Date uploadTime;

	/**
	 * 文件类型
	 * 0、文件夹
	 * 1、图片
	 * 2、视频
	 * 3、文档
	 * 4、音乐
	 * 5、种子
	 * 6、其他
	 */
	private Integer fileType;
}
