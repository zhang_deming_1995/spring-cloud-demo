package com.dm.cloud.api.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UploadInit implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 * 分片总数
	 */
	private Integer partCount;
	/**
	 * 第几个分片
	 */
	private List<Integer> uploaded;
	/**
	 * uploadId
	 */
	private String uploadId;
	/**
	 * 上传地址黎贝澳
	 */
	private List<String> uploadUrls;
}
