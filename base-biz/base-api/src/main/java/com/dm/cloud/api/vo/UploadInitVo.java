package com.dm.cloud.api.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UploadInitVo implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 * uploadId
	 */
	private String uploadId;
	/**
	 * 上传地址
	 */
	private List<String> uploadUrl;
	/**
	 * 是否完成
	 */
	private Boolean finished;
}
