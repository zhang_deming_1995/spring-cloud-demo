package com.dm.cloud.feign.base;

import com.dm.cloud.feign.fallback.BaseFeignFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "product-service",fallbackFactory = BaseFeignFallbackFactory.class)
public interface BaseFeign {

}