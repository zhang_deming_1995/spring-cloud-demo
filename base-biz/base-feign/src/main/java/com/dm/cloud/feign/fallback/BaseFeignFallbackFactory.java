package com.dm.cloud.feign.fallback;

import com.dm.cloud.feign.base.BaseFeign;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class BaseFeignFallbackFactory implements FallbackFactory<BaseFeign> {
    @Override
    public BaseFeign create(Throwable throwable) {
        BaseFeignFallback productFeignFallback = new BaseFeignFallback();
//        productFeignFallback.setCause(throwable);
        return productFeignFallback;
    }
}