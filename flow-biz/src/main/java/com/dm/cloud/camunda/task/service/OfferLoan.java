package com.dm.cloud.camunda.task.service;

import java.util.logging.Logger;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class OfferLoan implements JavaDelegate {

    private final static Logger LOGGER = Logger.getLogger("LOAN-REQUESTS");

    @Override
    public void execute(DelegateExecution execution) {
        LOGGER.info("offer " + execution.getVariable("lenderId") + " loans");
    }
}