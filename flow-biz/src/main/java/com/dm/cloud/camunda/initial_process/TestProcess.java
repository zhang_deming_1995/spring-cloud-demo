package com.dm.cloud.camunda.initial_process;

import org.apache.catalina.AccessLog;
import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.engine.DecisionService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.spring.boot.starter.event.PostDeployEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class TestProcess {
    // 增加logger用于打印决策后的结果
    protected final static Logger LOGGER = Logger.getLogger(TestProcess.class.getName());

    @Autowired
    private RuntimeService runtimeService;

    @EventListener
    private void processPostDeploy(PostDeployEvent event) {
//        runtimeService.startProcessInstanceByKey("loan_approval");
    }

    @EventListener
    private void processPostDeploy2(PostDeployEvent event) {
//        runtimeService.startProcessInstanceByKey("loan_approval");
        // >>>> 新增
        DecisionService decisionService = event.getProcessEngine().getDecisionService();
        // 输入参数
        VariableMap variables = Variables.createVariables()
                .putValue("bmi", 20.0);
//         调用决策
        DmnDecisionTableResult dmnDecisionTableResult = decisionService
                .evaluateDecisionTableByKey("bmi-classification", variables);
        String result = dmnDecisionTableResult.getSingleEntry();
        // 打印结果
        LOGGER.log(Level.INFO, "\n\nBMI Classification result is : {0}\n\n", result);
        // <<<<

        // >>>> 新增
        DmnDecisionTableResult suggestionResult = decisionService
                .evaluateDecisionTableByKey("bmi-suggestion", variables);
        String suggestion = suggestionResult.getSingleEntry();

        LOGGER.log(Level.INFO, "\n\nBMI Classification suggestion is : {0}\n\n", suggestion);
        // <<<<
    }
}
