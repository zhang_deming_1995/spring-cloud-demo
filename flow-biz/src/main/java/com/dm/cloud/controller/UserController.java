package com.dm.cloud.controller;

import com.dm.cloud.common.R;
import com.dm.cloud.pojo.camunda.DgcatCamundaUser;
import com.dm.cloud.service.CamundaIdentityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/camunda/user")
public class UserController {

    @Autowired
    CamundaIdentityService service;

    @GetMapping("/listUser")
    public R listUser(DgcatCamundaUser user){
        return R.success(service.listUser(user));
    }

    @PostMapping("/saveUser")
    public R saveUser(@RequestBody DgcatCamundaUser user){
        return R.success(service.saveUser(user));
    }

    @PostMapping("/deleteUser")
    public R deleteUser(@RequestBody DgcatCamundaUser user){
        service.deleteUser(user);
        return R.success();
    }
}
