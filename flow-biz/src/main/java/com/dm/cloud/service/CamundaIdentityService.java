package com.dm.cloud.service;

import com.dm.cloud.pojo.camunda.DgcatCamundaUser;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.identity.UserQuery;
import org.camunda.bpm.engine.impl.persistence.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CamundaIdentityService {

    @Autowired
    IdentityService identityService;

    public List<User> listUser(DgcatCamundaUser user) {
        List<User> users = new ArrayList<>();
        UserQuery query = identityService.createUserQuery();
        if(StringUtils.isNotBlank(user.getId())){
            query.userId(user.getId());
        }
        if(StringUtils.isNotBlank(user.getFirstName())){
            query.userFirstNameLike(user.getFirstName());
        }
        if(StringUtils.isNotBlank(user.getLastName())){
            query.userLastNameLike(user.getLastName());
        }
        if(StringUtils.isNotBlank(user.getEmail())){
            query.userEmailLike(user.getEmail());
        }
        query.orderByUserId().asc();
        return query.list();
    }

    /**
     * 保存用户信息
     * @param user
     * @return
     */
    public String saveUser(DgcatCamundaUser user){
        UserEntity userEntity = new UserEntity();
        userEntity.setId(user.getId());
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        userEntity.setPassword(user.getPassword());
        userEntity.setEmail(user.getEmail());
        identityService.saveUser(userEntity);
        return user.getId();
    }

    /**
     * 删除用户
     * @param user
     * @return
     */
    public void deleteUser(DgcatCamundaUser user){
        if(StringUtils.isBlank(user.getId())){
            throw new RuntimeException("缺少用户ID");
        }
        identityService.deleteUser(user.getId());
    }


}
