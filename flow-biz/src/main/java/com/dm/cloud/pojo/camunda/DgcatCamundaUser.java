package com.dm.cloud.pojo.camunda;

import lombok.Data;
import org.camunda.bpm.engine.identity.User;

@Data
public class DgcatCamundaUser implements User {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
