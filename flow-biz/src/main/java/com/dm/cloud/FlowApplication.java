package com.dm.cloud;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableProcessApplication // 新增
public class FlowApplication {

    @Autowired
    public static void main(String[] args) {
        SpringApplication.run(FlowApplication.class, args);
    }
}