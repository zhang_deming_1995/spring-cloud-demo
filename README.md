# SpringCloud-Alibaba-Demo

#### spring cloud 2021.0.4
#### spring cloud alibaba 2021.0.4.0
#### spring boot 2.6.11
#### alibaba sentinel 2021.0.4.0
#### alibaba nacos 2021.0.4.0
#### alibaba seata 2.2.0.RELEASE

#### 在线体验地址
#### http://182.92.201.143:3001/#/login
#### ceshi  123456 管理员
#### ceshi2 123456 
#### ceshi3 123456 
#### ceshi4 123456 

## 更新记录
### --2023.07.28
1.基于POI实现的导入导出工具类
### --2023.02.06
1.增加Kaptcha图形验证码方案实现
### --2023.01.04
1.增加基于sential、redis、mysql的订单秒杀实现方案
### --2022.11.15
1.组件版本升级 -- 用于兼容 nacos 2.x
### --2022.07.21
1.基于MINIO实现的大文件上传
2.在线聊天室
3.其他工具类
### --2021.09.24  
1.Rabbit MQ工具类
### --2021.09.22  
1.添加Elastic-Job服务
2.添加REDIS、ZK分布式锁工具类
### --2021.09.16  
1.增加MINIO上传下载工具类 使用minio相关配置要先启用custom.minio.enable = true
>custom:  
>>minio:  
>>>enable: true  
>>>url: http://${MINIO_HOST:x.x.x.x}:${MINIO_PORT:x}  
>>>secure: false  
>>>accessKey: ${MINIO_USER:xx}  
>>>secureKey: ${MINIO_PASS:xxxx}  
### --2021.09.15  
1.增加多数据源动态切换配置  
>添加common依赖 默认数据源cloud_base  
>>多数据源：启动类屏蔽数据源自动配置  
>>配置 custom.datasource.enable=true
>>*@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)  
### --2021.09.14  
1.添加自定义认证服务（短信验证码）  
2.添加Feign熔断，避免业务服务异常导致雪崩现象  
3.添加禁止直接访问后台服务功能  
### --2021.09.13  
1.openfeign调取服务前传递oauth认证令牌  
2.捕获服务端异常信息  
3.去中心化，更换jwt透明令牌认证  
### --2021.09.10  
1.增加OAuth认证  
2.上传数据库表结构  
### --2021.09.09  
1.整合SEATA分布式事务  
2.增加网关服务  
### --2021.09.07  
1.配置隔离  
### --2021.08.26  
1.代码上传  
